package genmock

import (
	"fmt"
	"sync"
	"time"
)

// TestingT interface for genereted mock type to pass *testing.T.
type TestingT interface {
	Fatalf(format string, args ...interface{})
	Helper()
}

type fatalfer interface {
	Fatalf(format string, args ...interface{})
}

type loggerHelper struct {
	fatalfer
}

func (l loggerHelper) Helper() {}

// NewLoggerHelper returns TestingT implemetner to pass logger instead *testing.T.
func NewLoggerHelper(f fatalfer) TestingT {
	return loggerHelper{fatalfer: f}
}

// AnythingArg argument of the method may be any of type and value.
type AnythingArg string

// Anything is a const to pass in expected argument value.
//
//	genmock.NewCall("MethodName", []interface{}{genmock.Anything}, nil, 1)
const Anything = AnythingArg("Anything")

// AnythingOfTypeArg argument of the method may be any of value but with mane of type.
type AnythingOfTypeArg string

// AnythingOfType is a func to wrap type name to use as expected argument in method.
//
//	genmock.NewCall("MethodName", []interface{}{genmock.AnythingOfType("int")}, nil, 1)
func AnythingOfType(typeName string) AnythingOfTypeArg {
	return AnythingOfTypeArg(typeName)
}

// IsEqual it is type just for documentation.
// You can use this function to compare arguments according to your own rules.
//
//	matcher := func(arg interface{}) bool { return arg.(int) == 3 }
//	genmock.NewCall("MethodName", []interface{}{matcher}, nil, 1)
type IsEqual func(interface{}) bool

// TimesAny means that the method can be called any number of times.
const TimesAny int = -1

// Call way to call method, contains expected arguments, number of calls and
// other results of the call. You can set differents expected arguments per call
// or you can set univarsal set of the arguments.
type Call struct {
	// Name of the method.
	MethodName string
	// Arguments of the method to compare with called actual arguments.
	Args []interface{}
	// Arguments of the method to retun when mock found this method call.
	Returns []interface{}
	// The number of times to call this caller of the method.
	// -1 any times, 0 already called, 1 ... n times.
	Times int

	// Call will be block until it receives a message or is closed.
	WaitFor <-chan time.Time
	// Call will be sleep this time before return arguments.
	WaitTime time.Duration
	// Runs func RunFn(args) when mock found this method call.
	// It's useful when you want to unmarshall method arguments.
	RunFn func(args []interface{})
	// Calls panic(*PanicMessage) when mock found this method call.
	PanicMsg *string
}

// NewCall takes basic call settings as a method name, expected arguments,
// returned arguments, and the number of possible retries.
// The other fields can be filled in manually.
func NewCall(methodName string, args, returns []interface{}, times int) *Call {
	return &Call{
		MethodName: methodName,
		Args:       args,
		Returns:    returns,
		Times:      times,

		PanicMsg: nil,
		RunFn:    nil,
		WaitFor:  nil,
		WaitTime: 0,
	}
}

type calls struct {
	mu sync.Mutex

	list []*Call
}

func (c *calls) push(call *Call) {
	c.mu.Lock()
	c.list = append(c.list, call)
	c.mu.Unlock()
}

func (c *calls) findExpectedCall(args ...interface{}) (*Call, error) {
	for _, call := range c.list {
		if call.Times == 0 {
			continue
		}

		if err := IsArgumentListsEquals(call.Args, args); err == nil {
			return call, nil
		}
	}

	return nil, ErrExpectedCallNotFound
}

func (c *calls) findSimilarCall(args ...interface{}) (*Call, error) {
	errs := make([]string, 0, len(c.list))

	for i, call := range c.list {
		if call.Times == 0 {
			continue
		}

		err := IsArgumentListsSimilar(call.Args, args)
		if err == nil {
			return call, nil
		}

		errs = append(errs, fmt.Sprintf("[%d]: %s", i, err))
	}

	return nil, fmtErrorWithList(ErrSimilarCallNotFound, errs)
}

func (c *calls) findCall(args ...interface{}) (*Call, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	call, err := c.findExpectedCall(args...) // We try to find first expected call with deep equals args.
	if err != nil {
		call, err = c.findSimilarCall(args...) // Now we try to find first similar call (Anything ...)
		if err != nil {
			return nil, err
		}
	}

	if call.Times > 0 {
		call.Times--
	}

	return call, nil
}

// Mock contains methods to add and call mocked interface methods.
type Mock struct {
	mu sync.Mutex

	methods map[string]*calls
}

// NewMock returns empty mock type.
// Feel free user default value of the Mock:
//
//	mock := genmock.NewMock()
//	mock := new(genmock.Mock)
func NewMock() *Mock {
	return &Mock{
		methods: make(map[string]*calls),
	}
}

// AddCall adds your expected call variant of method to list of methods.
//
//	mock := genmock.NewMock().AddCall(mockMethod1).AddCall(mockMethod2)
func (m *Mock) AddCall(c *Call) *Mock {
	m.mu.Lock()

	if m.methods == nil {
		m.methods = make(map[string]*calls)
	}

	list, ok := m.methods[c.MethodName]
	if !ok {
		list = new(calls)
		m.methods[c.MethodName] = list
	}

	list.push(c)

	m.mu.Unlock()

	return m
}

// MethodCalled tries to find method by name and actual arguments.
// First of all trying to find call with full equals arguments.
// If failed then trying to find similar call with Anything and AnythingOfType args.
// Returns the returned arguments and checks another
// call fields and calls them before returning the arguments.
//
//	// Method of the interface SetData(data sting) error
//	setData := genmock.NewCall("SetData", []interface{}{genmock.AnythingOfType("string")}, []interface{}{nil}, 1)
//	mock := genmock.NewMock().AddCall(setData)
//	ret, err := mock.MethodCalled("SetData", "some text data")
func (m *Mock) MethodCalled(methodName string, args ...interface{}) ([]interface{}, error) {
	m.mu.Lock()
	calls, ok := m.methods[methodName]
	m.mu.Unlock()

	if !ok {
		return nil, fmt.Errorf("%w: %q", ErrMethodUnknown, methodName)
	}

	call, err := calls.findCall(args...)
	if err != nil {
		return nil, fmt.Errorf("failed to find call for method %q with args %s: %w",
			methodName, fmtSliceInterfaces(args), err)
	}

	if call.WaitFor != nil {
		<-call.WaitFor
	}

	time.Sleep(call.WaitTime)

	if call.RunFn != nil {
		call.RunFn(args)
	}

	if call.PanicMsg != nil {
		panic(*call.PanicMsg)
	}

	return call.Returns, nil
}

// AssertExpectations checks that all added methods was called or with TimesAny parameter.
// Returns an error with a full description of not called methods.
func (m *Mock) AssertExpectations() error {
	m.mu.Lock()
	defer m.mu.Unlock()

	methods := []string{}

	for _, calls := range m.methods {
		calls.mu.Lock()

		for _, call := range calls.list {
			if call.Times > 0 {
				methods = append(methods, fmt.Sprintf("%q %d time(s) left", call.MethodName, call.Times))
			}
		}

		calls.mu.Unlock()
	}

	if len(methods) == 0 {
		return nil
	}

	return fmtErrorWithList(ErrRemainsSeveralTimesCallMethods, methods)
}

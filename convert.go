package genmock

// ConvertArgError converts interface argument by index to error type.
func ConvertArgError(index int, args []interface{}) (res, err error) {
	if len(args) <= index {
		return nil, fmtErrorIndexOutOfRange(len(args), index)
	}

	arg := args[index]

	if arg == nil { // error may be nil and you can't to conver it to error.
		return nil, nil
	}

	res, ok := arg.(error)
	if !ok {
		return nil, fmtErrorUnexpectedArgumentType("error", arg)
	}

	return res, nil
}

// ConvertArgBool converts interface argument by index to bool type.
func ConvertArgBool(index int, args []interface{}) (bool, error) {
	if len(args) <= index {
		return false, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(bool)
	if !ok {
		return false, fmtErrorUnexpectedArgumentType("bool", args[index])
	}

	return res, nil
}

// ConvertArgString converts interface argument by index to string type.
func ConvertArgString(index int, args []interface{}) (string, error) {
	if len(args) <= index {
		return "", fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(string)
	if !ok {
		return "", fmtErrorUnexpectedArgumentType("string", args[index])
	}

	return res, nil
}

// ConvertArgInt converts interface argument by index to int type.
func ConvertArgInt(index int, args []interface{}) (int, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(int)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("int", args[index])
	}

	return res, nil
}

// ConvertArgInt8 converts interface argument by index to int8 type.
func ConvertArgInt8(index int, args []interface{}) (int8, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(int8)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("int8", args[index])
	}

	return res, nil
}

// ConvertArgInt16 converts interface argument by index to int16 type.
func ConvertArgInt16(index int, args []interface{}) (int16, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(int16)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("int16", args[index])
	}

	return res, nil
}

// ConvertArgInt32 converts interface argument by index to int32 type.
func ConvertArgInt32(index int, args []interface{}) (int32, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(int32)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("int32", args[index])
	}

	return res, nil
}

// ConvertArgInt64 converts interface argument by index to int64 type.
func ConvertArgInt64(index int, args []interface{}) (int64, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(int64)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("int64", args[index])
	}

	return res, nil
}

// ConvertArgUint converts interface argument by index to uint type.
func ConvertArgUint(index int, args []interface{}) (uint, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(uint)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("uint", args[index])
	}

	return res, nil
}

// ConvertArgUint8 converts interface argument by index to uint8 type.
func ConvertArgUint8(index int, args []interface{}) (uint8, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(uint8)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("uint8", args[index])
	}

	return res, nil
}

// ConvertArgUint16 converts interface argument by index to uint16 type.
func ConvertArgUint16(index int, args []interface{}) (uint16, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(uint16)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("uint16", args[index])
	}

	return res, nil
}

// ConvertArgUint32 converts interface argument by index to uint32 type.
func ConvertArgUint32(index int, args []interface{}) (uint32, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(uint32)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("uint32", args[index])
	}

	return res, nil
}

// ConvertArgUint64 converts interface argument by index to uint64 type.
func ConvertArgUint64(index int, args []interface{}) (uint64, error) {
	if len(args) <= index {
		return 0, fmtErrorIndexOutOfRange(len(args), index)
	}

	res, ok := args[index].(uint64)
	if !ok {
		return 0, fmtErrorUnexpectedArgumentType("uint64", args[index])
	}

	return res, nil
}

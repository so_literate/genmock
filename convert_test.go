package genmock_test

import (
	"errors"
	"testing"

	"gitlab.com/so_literate/genmock"
)

func TestConvertArgError(t *testing.T) {
	t.Parallel()

	testCase := func(expErr, expVal error, args ...interface{}) {
		val, err := genmock.ConvertArgError(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if !errors.Is(val, expVal) {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, errTest, errTest)
	testCase(nil, nil, nil)
	testCase(nil, nil, []interface{}{error(nil)}...)
	testCase(nil, nil, []interface{}{nil}...)
	testCase(genmock.ErrIndexOutOfRange, nil)
	testCase(genmock.ErrUnexpectedArgumentType, nil, 1)
}

func TestConvertArgBool(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal bool, args ...interface{}) {
		val, err := genmock.ConvertArgBool(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, true, true)
	testCase(nil, false, false)
	testCase(genmock.ErrIndexOutOfRange, false)
	testCase(genmock.ErrUnexpectedArgumentType, false, 1)
}

func TestConvertArgString(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal string, args ...interface{}) {
		val, err := genmock.ConvertArgString(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, "text", "text")
	testCase(nil, "", "")
	testCase(genmock.ErrIndexOutOfRange, "")
	testCase(genmock.ErrUnexpectedArgumentType, "", 1)
}

func TestConvertArgInt(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal int, args ...interface{}) {
		val, err := genmock.ConvertArgInt(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, 1)
	testCase(nil, 0, 0)
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgInt8(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal int8, args ...interface{}) {
		val, err := genmock.ConvertArgInt8(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, int8(1))
	testCase(nil, 0, int8(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgInt16(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal int16, args ...interface{}) {
		val, err := genmock.ConvertArgInt16(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, int16(1))
	testCase(nil, 0, int16(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgInt32(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal int32, args ...interface{}) {
		val, err := genmock.ConvertArgInt32(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, int32(1))
	testCase(nil, 0, int32(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgInt64(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal int64, args ...interface{}) {
		val, err := genmock.ConvertArgInt64(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, int64(1))
	testCase(nil, 0, int64(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgUint(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal uint, args ...interface{}) {
		val, err := genmock.ConvertArgUint(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, uint(1))
	testCase(nil, 0, uint(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgUint8(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal uint8, args ...interface{}) {
		val, err := genmock.ConvertArgUint8(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, uint8(1))
	testCase(nil, 0, uint8(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgUint16(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal uint16, args ...interface{}) {
		val, err := genmock.ConvertArgUint16(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, uint16(1))
	testCase(nil, 0, uint16(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgUint32(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal uint32, args ...interface{}) {
		val, err := genmock.ConvertArgUint32(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, uint32(1))
	testCase(nil, 0, uint32(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

func TestConvertArgUint64(t *testing.T) {
	t.Parallel()

	testCase := func(expErr error, expVal uint64, args ...interface{}) {
		val, err := genmock.ConvertArgUint64(0, args)
		if !errors.Is(err, expErr) {
			t.Fatalf("wrong actual error: want: %#v | got: %#v", expErr, err)
		}

		if expVal != val {
			t.Fatalf("wrong actual value: want %#v | got: %#v", expVal, val)
		}
	}

	testCase(nil, 1, uint64(1))
	testCase(nil, 0, uint64(0))
	testCase(genmock.ErrIndexOutOfRange, 0)
	testCase(genmock.ErrUnexpectedArgumentType, 0, "1")
}

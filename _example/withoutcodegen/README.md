Without Code Gen example
========

Example of the using genmock for unit tests without code generator.

# Legend

- You have a package with business logic ('withoutcodegen.go' - Logic)
- Type Logic contains a dependency 'Database' as an interafce
- You created your own Database implementer ('withoutcodegen_test.go' - testDB)
- Now you can write Unit tests with that Database implementer

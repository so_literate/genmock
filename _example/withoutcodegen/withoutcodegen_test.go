package withoutcodegen_test

import (
	"testing"

	"gitlab.com/so_literate/genmock"

	"gitlab.com/so_literate/genmock/_example/codegen"
)

type testDB struct {
	mock *genmock.Mock
	t    genmock.TestingT
}

func (db *testDB) GetData(key string) (int64, error) {
	res, err := db.mock.MethodCalled("GetData", key)
	if err != nil {
		db.t.Fatalf("mock.MethodCalled: %v", err)
	}

	v, err := genmock.ConvertArgInt64(0, res)
	if err != nil {
		db.t.Fatalf("genmock.ConvertArgInt64: %v", err)
	}

	resErr, err := genmock.ConvertArgError(1, res)
	if err != nil {
		db.t.Fatalf("genmock.ConvertArgError: %v", err)
	}

	return v, resErr
}

func TestGet(t *testing.T) {
	t.Parallel()

	db := &testDB{
		mock: genmock.NewMock(),
		t:    t,
	}

	logic := codegen.NewLogic(db)

	db.mock.AddCall(genmock.NewCall(
		"GetData",
		[]interface{}{"key"},
		[]interface{}{int64(1), nil},
		1,
	))

	value, err := logic.Get("key")
	if err != nil {
		t.Fatal(err)
	}

	if value != 1 {
		t.Fatalf("wrong value from db: %v", value)
	}

	if err = db.mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}
}

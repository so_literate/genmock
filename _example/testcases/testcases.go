package testcases

type Another interface {
	Method() string
}

//go:generate genmock -search.name=Iface -print.file.test -print.place.in_package -print.package.test
type Iface interface {
	SingleValue(value int) error
	VariadicVaules(values ...int) error
	VariadicEmpty(...int) error
	VariadicInterfaces(v ...interface{}) error
	Anothers(a ...Another) error
}

type MockUser struct {
	Iface Iface

	Value         int
	Values        []int
	AnotherIfaces []Another
}

func (m *MockUser) SingleValue() error {
	return m.Iface.SingleValue(m.Value)
}

func (m *MockUser) VariadicVaules() error {
	return m.Iface.VariadicVaules(m.Values...)
}

func (m *MockUser) VariadicEmpty() error {
	return m.Iface.VariadicEmpty()
}

func (m *MockUser) VariadicInterfaces() error {
	ifaces := make([]interface{}, len(m.Values))
	for i, v := range m.Values {
		ifaces[i] = v
	}
	return m.Iface.VariadicInterfaces(ifaces...)
}

func (m *MockUser) Anothers() error {
	return m.Iface.Anothers(m.AnotherIfaces...)
}

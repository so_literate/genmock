// Code generated by genmock. DO NOT EDIT.
//  genmock version: v0.0.5

// Package testcases_test contains mock implementer of the Iface interface.
package testcases_test

import (
	"fmt"

	"gitlab.com/so_literate/genmock"
	"gitlab.com/so_literate/genmock/_example/testcases"
)

// Iface_Mock implements mock of the Iface interface.
type Iface_Mock struct {
	Mock *genmock.Mock
	t    genmock.TestingT

	ReturnMockErrorAsResult bool
}

// NewIface_Mock returns a new mock of Iface interface implementer.
// Takes *testing.T to stop failed testing process.
func NewIface_Mock(t genmock.TestingT) *Iface_Mock {
	return &Iface_Mock{
		Mock: genmock.NewMock(),
		t:    t,
	}
}

func (_m *Iface_Mock) Anothers(a ...testcases.Another) error {
	_m.t.Helper()

	_ret, _err := _m.Mock.MethodCalled("Anothers", a)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(Anothers) call mock method: %w", _err)
		}
		_m.t.Fatalf("(Anothers) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(Anothers) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(Anothers) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(Anothers) check returned type: %w", _err)
			}
			_m.t.Fatalf("(Anothers) check returned type: %s", _err)
		}
	}

	return _r0
}

func (_m *Iface_Mock) SingleValue(value int) error {
	_m.t.Helper()

	_ret, _err := _m.Mock.MethodCalled("SingleValue", value)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(SingleValue) call mock method: %w", _err)
		}
		_m.t.Fatalf("(SingleValue) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(SingleValue) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(SingleValue) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(SingleValue) check returned type: %w", _err)
			}
			_m.t.Fatalf("(SingleValue) check returned type: %s", _err)
		}
	}

	return _r0
}

func (_m *Iface_Mock) VariadicEmpty(_a0 ...int) error {
	_m.t.Helper()

	_ret, _err := _m.Mock.MethodCalled("VariadicEmpty", _a0)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicEmpty) call mock method: %w", _err)
		}
		_m.t.Fatalf("(VariadicEmpty) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicEmpty) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(VariadicEmpty) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(VariadicEmpty) check returned type: %w", _err)
			}
			_m.t.Fatalf("(VariadicEmpty) check returned type: %s", _err)
		}
	}

	return _r0
}

func (_m *Iface_Mock) VariadicInterfaces(v ...interface{}) error {
	_m.t.Helper()

	_ret, _err := _m.Mock.MethodCalled("VariadicInterfaces", v)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicInterfaces) call mock method: %w", _err)
		}
		_m.t.Fatalf("(VariadicInterfaces) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicInterfaces) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(VariadicInterfaces) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(VariadicInterfaces) check returned type: %w", _err)
			}
			_m.t.Fatalf("(VariadicInterfaces) check returned type: %s", _err)
		}
	}

	return _r0
}

func (_m *Iface_Mock) VariadicVaules(values ...int) error {
	_m.t.Helper()

	_ret, _err := _m.Mock.MethodCalled("VariadicVaules", values)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicVaules) call mock method: %w", _err)
		}
		_m.t.Fatalf("(VariadicVaules) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(VariadicVaules) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(VariadicVaules) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(VariadicVaules) check returned type: %w", _err)
			}
			_m.t.Fatalf("(VariadicVaules) check returned type: %s", _err)
		}
	}

	return _r0
}

// Iface_MockSet_Anothers allows to set arguments and results of the mock call Anothers.
type Iface_MockSet_Anothers struct {
	Call *genmock.Call
}

// On_Anothers adds a call of the Method to mock.
func (_m *Iface_Mock) On_Anothers() Iface_MockSet_Anothers {
	call := genmock.NewCall(
		"Anothers",
		[]interface{}{genmock.AnythingOfType("[]testcases.Another")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_Anothers{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_Anothers) Args(a ...testcases.Another) Iface_MockSet_Anothers {
	_s.Call.Args[0] = a
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_Anothers) ArgsAnything(a interface{}) Iface_MockSet_Anothers {
	_s.Call.Args[0] = a
	return _s
}

// Arg_a_Matcher sets matcher of the a argument value.
func (_s Iface_MockSet_Anothers) Arg_a_Matcher(matcher func(a []testcases.Another) bool) Iface_MockSet_Anothers {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.([]testcases.Another)
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_Anothers) Rets(_r0 error) Iface_MockSet_Anothers {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_Anothers) Times(times int) Iface_MockSet_Anothers {
	_s.Call.Times = times
	return _s
}

// Iface_MockSet_SingleValue allows to set arguments and results of the mock call SingleValue.
type Iface_MockSet_SingleValue struct {
	Call *genmock.Call
}

// On_SingleValue adds a call of the Method to mock.
func (_m *Iface_Mock) On_SingleValue() Iface_MockSet_SingleValue {
	call := genmock.NewCall(
		"SingleValue",
		[]interface{}{genmock.AnythingOfType("int")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_SingleValue{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_SingleValue) Args(value int) Iface_MockSet_SingleValue {
	_s.Call.Args[0] = value
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_SingleValue) ArgsAnything(value interface{}) Iface_MockSet_SingleValue {
	_s.Call.Args[0] = value
	return _s
}

// Arg_value_Matcher sets matcher of the value argument value.
func (_s Iface_MockSet_SingleValue) Arg_value_Matcher(matcher func(value int) bool) Iface_MockSet_SingleValue {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.(int)
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_SingleValue) Rets(_r0 error) Iface_MockSet_SingleValue {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_SingleValue) Times(times int) Iface_MockSet_SingleValue {
	_s.Call.Times = times
	return _s
}

// Iface_MockSet_VariadicEmpty allows to set arguments and results of the mock call VariadicEmpty.
type Iface_MockSet_VariadicEmpty struct {
	Call *genmock.Call
}

// On_VariadicEmpty adds a call of the Method to mock.
func (_m *Iface_Mock) On_VariadicEmpty() Iface_MockSet_VariadicEmpty {
	call := genmock.NewCall(
		"VariadicEmpty",
		[]interface{}{genmock.AnythingOfType("[]int")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_VariadicEmpty{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_VariadicEmpty) Args(_a0 ...int) Iface_MockSet_VariadicEmpty {
	_s.Call.Args[0] = _a0
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_VariadicEmpty) ArgsAnything(_a0 interface{}) Iface_MockSet_VariadicEmpty {
	_s.Call.Args[0] = _a0
	return _s
}

// Arg_a0_Matcher sets matcher of the _a0 argument value.
func (_s Iface_MockSet_VariadicEmpty) Arg_a0_Matcher(matcher func(_a0 []int) bool) Iface_MockSet_VariadicEmpty {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.([]int)
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_VariadicEmpty) Rets(_r0 error) Iface_MockSet_VariadicEmpty {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_VariadicEmpty) Times(times int) Iface_MockSet_VariadicEmpty {
	_s.Call.Times = times
	return _s
}

// Iface_MockSet_VariadicInterfaces allows to set arguments and results of the mock call VariadicInterfaces.
type Iface_MockSet_VariadicInterfaces struct {
	Call *genmock.Call
}

// On_VariadicInterfaces adds a call of the Method to mock.
func (_m *Iface_Mock) On_VariadicInterfaces() Iface_MockSet_VariadicInterfaces {
	call := genmock.NewCall(
		"VariadicInterfaces",
		[]interface{}{genmock.AnythingOfType("[]interface {}")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_VariadicInterfaces{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_VariadicInterfaces) Args(v ...interface{}) Iface_MockSet_VariadicInterfaces {
	_s.Call.Args[0] = v
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_VariadicInterfaces) ArgsAnything(v interface{}) Iface_MockSet_VariadicInterfaces {
	_s.Call.Args[0] = v
	return _s
}

// Arg_v_Matcher sets matcher of the v argument value.
func (_s Iface_MockSet_VariadicInterfaces) Arg_v_Matcher(matcher func(v []interface{}) bool) Iface_MockSet_VariadicInterfaces {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.([]interface{})
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_VariadicInterfaces) Rets(_r0 error) Iface_MockSet_VariadicInterfaces {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_VariadicInterfaces) Times(times int) Iface_MockSet_VariadicInterfaces {
	_s.Call.Times = times
	return _s
}

// Iface_MockSet_VariadicVaules allows to set arguments and results of the mock call VariadicVaules.
type Iface_MockSet_VariadicVaules struct {
	Call *genmock.Call
}

// On_VariadicVaules adds a call of the Method to mock.
func (_m *Iface_Mock) On_VariadicVaules() Iface_MockSet_VariadicVaules {
	call := genmock.NewCall(
		"VariadicVaules",
		[]interface{}{genmock.AnythingOfType("[]int")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_VariadicVaules{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_VariadicVaules) Args(values ...int) Iface_MockSet_VariadicVaules {
	_s.Call.Args[0] = values
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_VariadicVaules) ArgsAnything(values interface{}) Iface_MockSet_VariadicVaules {
	_s.Call.Args[0] = values
	return _s
}

// Arg_values_Matcher sets matcher of the values argument value.
func (_s Iface_MockSet_VariadicVaules) Arg_values_Matcher(matcher func(values []int) bool) Iface_MockSet_VariadicVaules {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.([]int)
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_VariadicVaules) Rets(_r0 error) Iface_MockSet_VariadicVaules {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_VariadicVaules) Times(times int) Iface_MockSet_VariadicVaules {
	_s.Call.Times = times
	return _s
}

package testcases_test

import (
	"testing"

	"gitlab.com/so_literate/genmock"

	"gitlab.com/so_literate/genmock/_example/testcases"
)

type anotherImpl string

func (a anotherImpl) Method() string {
	return string(a)
}

var (
	value    = 1
	values   = []int{1, 2, 3}
	anothers = []testcases.Another{anotherImpl("1"), anotherImpl("2")}
)

func getMockUser(t *testing.T) (*Iface_Mock, *testcases.MockUser) {
	iface := NewIface_Mock(t)
	iface.ReturnMockErrorAsResult = true

	user := &testcases.MockUser{
		Iface:         iface,
		Value:         value,
		Values:        values,
		AnotherIfaces: anothers,
	}

	return iface, user
}

func TestGenMockWork(t *testing.T) {
	t.Parallel()

	iface, user := getMockUser(t)

	assert := func() {
		t.Helper()

		if err := user.SingleValue(); err != nil {
			t.Fatalf("SingleValue: %s", err)
		}

		if err := user.VariadicVaules(); err != nil {
			t.Fatalf("VariadicVaules: %s", err)
		}

		if err := user.VariadicEmpty(); err != nil {
			t.Fatalf("VariadicEmpty: %s", err)
		}

		if err := user.VariadicInterfaces(); err != nil {
			t.Fatalf("VariadicInterfaces: %s", err)
		}

		if err := user.Anothers(); err != nil {
			t.Fatalf("Anothers: %s", err)
		}

		if err := iface.Mock.AssertExpectations(); err != nil {
			t.Fatalf("AssertExpectations: %s", err)
		}
	}

	// Default expected arguments with soft comparing but work well.
	iface.On_SingleValue()
	iface.On_VariadicVaules()
	iface.On_VariadicEmpty()
	iface.On_VariadicInterfaces()
	iface.On_Anothers()

	assert()

	// Anything in args.
	iface.On_SingleValue().ArgsAnything(genmock.Anything)
	iface.On_VariadicVaules().ArgsAnything(genmock.Anything)
	iface.On_VariadicEmpty().ArgsAnything(genmock.Anything)
	iface.On_VariadicInterfaces().ArgsAnything(genmock.Anything)
	iface.On_Anothers().ArgsAnything(genmock.Anything)

	assert()

	// Seted arguments
	iface.On_SingleValue().Args(value)
	iface.On_VariadicVaules().Args(values...)
	iface.On_VariadicEmpty().Args()
	iface.On_VariadicInterfaces().Args(values[0], values[1], values[2])
	iface.On_Anothers().Args(anotherImpl("1"), anotherImpl("2"))

	assert()

	// Matchers
	iface.On_SingleValue().Arg_value_Matcher(func(v int) bool { return v == value })
	iface.On_VariadicVaules().Arg_values_Matcher(func(vs []int) bool { return vs[0] == values[0] && vs[1] == values[1] && vs[2] == values[2] })
	iface.On_VariadicEmpty().Arg_a0_Matcher(func(vs []int) bool { return vs == nil })
	iface.On_VariadicInterfaces().Arg_v_Matcher(func(vs []interface{}) bool { return vs[0] == values[0] && vs[1] == values[1] && vs[2] == values[2] })
	iface.On_Anothers().Arg_a_Matcher(func(a []testcases.Another) bool { return a[0] == anothers[0] && a[1] == anothers[1] })

	assert()

}

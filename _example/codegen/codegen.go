package codegen

import (
	"fmt"
)

//go:generate genmock -search.name=Database
type Database interface {
	GetData(key string) (v int64, err error)
}

type Logic struct {
	db Database
}

func NewLogic(db Database) *Logic {
	return &Logic{db: db}
}

func (l *Logic) Get(key string) (int64, error) {
	value, err := l.db.GetData(key)
	if err != nil {
		return 0, fmt.Errorf("db.GetData: %w", err)
	}

	fmt.Println("called db.GetData in logic", value)

	return value, nil
}

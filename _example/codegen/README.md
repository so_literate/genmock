Code Gen example
========

Example of the using code generator for unit tests.

# Legent

- You have a package with business logic ('codegen.go' - Logic)
- Type Logic contains a dependency 'Database' as an interafce.
- You added the comment into interafce `//go:generate genmock -search.name=Database`
- After that you ran `go generate ./...`
- Genmock was created the 'mock' directory with Database implementer
- Now you can write your Unit tests with mock of the Database interface (codegen_test.go)


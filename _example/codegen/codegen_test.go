package codegen_test

import (
	"testing"

	"gitlab.com/so_literate/genmock/_example/codegen"
	"gitlab.com/so_literate/genmock/_example/codegen/mock"
)

func TestGet(t *testing.T) {
	t.Parallel()

	db := mock.NewDatabase_Mock(t)
	db.ReturnMockErrorAsResult = true

	logic := codegen.NewLogic(db)

	db.On_GetData().Args("key").Rets(1, nil).Times(1)

	value, err := logic.Get("key")
	if err != nil {
		t.Fatal(err)
	}

	if value != 1 {
		t.Fatalf("wrong value from db: %v", value)
	}

	// mock db will return error about "call not found" as result of the GetData method (ReturnMockErrorAsResult).
	_, err = logic.Get("key")
	if err == nil {
		t.Fatalf("unexpected empty error: %v", err)
	}

	t.Log(err)

	// Check that all method called.
	if err = db.Mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}
}

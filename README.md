genmock
========

Golang mocking system with code generator to check 
the types of the arguments at the compilation stage.

# Features

* Dependency-free in generated mock code
* Generated code contains methods and typed arguments in builder style
* No panic in the test code:
* * All test checkers calls t.Fatal on unexpected behaviour
* * Or it can to return the mock error as a result of the method if it possible
* Flexible code generator, see config of the generator
* Also you can use the package genmock without code generator

See examples [here](https://gitlab.com/so_literate/genmock/-/tree/main/_example).

# Install

```sh
go install gitlab.com/so_literate/genmock/cmd/genmock@latest
```

# Code generator

```go
// This interface:
type Iface interface {
	Method(i int64) error
}

// You can test like this:
iface := mock.NewIface_Mock(t)

iface.On_Method().Args(1).Rets(nil).Times(genmock.TimesAny)

// All methods Args and Rets have typed arguments:
//   method builder -> Args(i int64).Rets(_r0 error)
//
// Also, you have flexible methods to pass interface or custom matcher:
//   ArgsAnything(i interface{}) or Arg_i_Matcher(matcher func(i int64) bool)
//
// Using strongly typed methods ensures that changes are checked at compile time.
// You can use Go compilation power instead panic at runtime.
```

# Using

## Recommended way

### 1. Find your interface in your code and add magic comment

```go
//go:generate genmock -search.name=SomeIface
type SomeIface interface {
	Method()
}
```

You can skip -search.name= falg if you have only one interface in the package.

### 2. Run `go generate` command

```sh
go generate ./...
```

### 3. Got your mocked interface here:
- in package `mock` near of your interface
- in file: `./mock/some_iface.go`
- with `mock` package name

### 4. Now you can import the mock package in your test files:

```go
package pkg_test

import (
	"testing"
	
	"host/user/project/pkg/mock"
)

func TestIface(t *testing.T) {
	someIface := mock.NewSomeIface_Mock(t)
	
	someIface.On_Method().Times(1)
	
	if err := someIface.Mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}
}
```

This is a good way, if you follow the best practices for writing interfaces in go:
> Keep interfaces small (the bigger the interface, the weaker abstraction)

You also have documentation in the code to generate the mock of the interface,
not in Makefile or in another build script.

## Generate test code in the package

You can generate mock code as your own test code of the package.

Add additional keys to genmock util:

```
//go:generate genmock -search.name=SomeIface -print.file.test -print.place.in_package -print.package.test
```

Now you got generated mock code in the package:
- file in the package: `some_iface_test.go`
- package name will be `pkg_test`

After that you can write your own test code using generated code without import of the `mock` package.

## Generate interfaces over project

You can use the genmock to scan the entire project code. 

### Generator and Usage example:

```sh
genmock -search.recursive -print.place.keep_tree
```

Genmock will create directory `./mock` with full import path of the each interface.

You can set some directories to skip in scan proccess `-search.skip_paths=./vendor`.

```go
import "host/user/project/mock/host/user/project/pkg"
```

# Config

You can configure the genmock in various ways.

## 1. Flags

Type `genmock -h` to see flag help message.

It contains 4 partitions:
- global: version, timeout (1 minute), config, h, help
- log: debug, no_color, pretty (true), quiet
- search: root ('.'), name ('.*'), skip_paths, recursive, include_tests, include_not_types
- print:
- - file: case (snake), test
- - place: path ('./mock'), in_package, keep_tree, stdout
- - package: name ('mock'), test
- - content: hide_version, tags

## 2. Environment variables

```
GENMOCK_LOG_DEBUG="false"
GENMOCK_LOG_QUIET="false"
GENMOCK_LOG_PRETTY="true"
GENMOCK_LOG_NO_COLOR="false"
GENMOCK_SEARCH_NAME=".*"
GENMOCK_SEARCH_ROOT="."
GENMOCK_SEARCH_RECURSIVE="false"
GENMOCK_SEARCH_SKIP_PATHS=""
GENMOCK_SEARCH_INCLUDE_TESTS="false"
GENMOCK_SEARCH_INCLUDE_NOT_TYPES="false"
GENMOCK_PRINT_FILE_CASE="snake"
GENMOCK_PRINT_FILE_TEST="false"
GENMOCK_PRINT_PLACE_PATH="./mock"
GENMOCK_PRINT_PLACE_KEEP_TREE="false"
GENMOCK_PRINT_PLACE_IN_PACKAGE="false"
GENMOCK_PRINT_PLACE_STDOUT="false"
GENMOCK_PRINT_PACKAGE_NAME="mock"
GENMOCK_PRINT_PACKAGE_TEST="false"
GENMOCK_PRINT_CONTENT_HIDE_VERSION="false"
GENMOCK_PRINT_CONTENT_TAGS=""
GENMOCK_TIMEOUT="1m"
GENMOCK_VERSION="false"
```

## 3. Config file

Pass you config file in -config flag `genmock -config=/path/to/config.json`

File example:
```
{
  "Log": {
    "Debug": false,
    "Quiet": false,
    "Pretty": true,
    "NoColor": false
  },
  "Search": {
    "Name": ".*",
    "Root": ".",
    "Recursive": false,
    "SkipPaths": [],
    "IncludeTests": false,
    "IncludeNotTypes": false
  },
  "Print": {
    "File": {
      "Case": "snake",
      "Test": false
    },
    "Place": {
      "Path": "./mock",
      "KeepTree": false,
      "InPackage": false,
      "Stdout": false
    },
    "Package": {
      "Name": "mock",
      "Test": false
    },
    "Content": {
      "HideVersion": false,
      "Tags": ""
    }
  },
  "Timeout": 60000000000,
  "Version": false
}
```

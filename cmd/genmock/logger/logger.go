// Package logger contains method to create logger for generator base on config.
package logger

import (
	"io"
	"os"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
)

// New returns new logger based on config.
func New(conf *config.Log) *zerolog.Logger {
	var writer io.Writer

	if conf.Pretty {
		writer = zerolog.ConsoleWriter{
			Out:        os.Stderr,
			NoColor:    conf.NoColor,
			TimeFormat: time.RFC3339,
		}
	} else {
		writer = os.Stderr
	}

	logger := zerolog.New(writer).Level(zerolog.InfoLevel).With().Timestamp().Logger()

	if conf.Debug {
		logger = logger.Level(zerolog.DebugLevel)
	}

	if conf.Quiet {
		logger = logger.Level(zerolog.Disabled)
	}

	return &logger
}

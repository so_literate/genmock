package logger_test

import (
	"bufio"
	"bytes"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/logger"
)

func prepareLogger(conf *config.Log) (*bytes.Buffer, *zerolog.Logger) {
	buf := new(bytes.Buffer)

	log := logger.New(conf)
	newLog := log.Output(buf)

	return buf, &newLog
}

func getLines(buf *bytes.Buffer) []string {
	lines := make([]string, 0)

	sc := bufio.NewScanner(buf)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}

	return lines
}

func TestNewLogger(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	testCase := func(line string, expected ...string) {
		for _, exp := range expected {
			so.Truef(strings.Contains(line, exp), "line: want %s, got %s", exp, line)
		}
	}

	conf := &config.Log{
		Debug:   false,
		Quiet:   false,
		Pretty:  false,
		NoColor: true,
	}

	// Without debug level
	buf, log := prepareLogger(conf)

	log.Debug().Msg("debug")
	log.Info().Msg("info")

	lines := getLines(buf)

	so.Len(lines, 1)
	testCase(lines[0], `"level":"info"`, `"message":"info"`)

	// With debug level
	conf.Debug = true
	buf, log = prepareLogger(conf)

	log.Debug().Msg("debug")

	lines = getLines(buf)

	so.Len(lines, 1)
	testCase(lines[0], `"level":"debug"`, `"message":"debug"`)

	// Quiet logger (debug enabled)
	conf.Quiet = true
	buf, log = prepareLogger(conf)

	log.Debug().Msg("debug")
	log.Info().Msg("info")
	log.Error().Msg("error")

	so.Equal("", buf.String())

	// Pretty logger, look in the terminal
	conf.Quiet = false
	conf.NoColor = false
	conf.Pretty = true
	log = logger.New(conf)

	log.Info().Msg("info")
}

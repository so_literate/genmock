package analyzer_test

import (
	"bytes"
	"context"
	"go/types"
	"io/fs"
	"path/filepath"
	"regexp/syntax"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/analyzer"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

func getLogger() (*bytes.Buffer, *zerolog.Logger) {
	buf := bytes.NewBuffer([]byte("\n"))
	log := zerolog.New(buf)
	return buf, &log
}

const packageTestdata = "gitlab.com/so_literate/genmock/cmd/genmock/testdata"

var (
	dataNamedArgument = model.Argument{
		IsPointer: &model.ArgumentPointer{
			Element: &model.Argument{
				IsNamed: &model.Argument{
					IsStruct: &model.ArgumentStruct{
						Fields: []*model.ArgumentStructField{},
					},

					Package:  packageTestdata + "/model",
					TypeName: "Data",
				},
			},
		},
	}

	cleanNamedArgument = model.Argument{
		IsNamed: &model.Argument{
			IsStruct: &model.ArgumentStruct{
				Fields: []*model.ArgumentStructField{}, // Without fields because it is named struct.
			},
			Package:  packageTestdata,
			TypeName: "Clean",
		},
	}

	errorNamedArgument = model.Argument{
		TypeName: "error",
		IsInterface: &model.ArgumentInterface{
			Methods: []*model.Method{},
		},
	}

	expectedMethods = []*model.Method{
		{
			Name: "MethodOfEmbeddedInterface", // real method: MethodOfEmbeddedInterface(int)
			Params: []*model.Argument{
				{TypeName: "int", BasicInfo: types.IsInteger},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Read", // real method: Read(p []byte) (n int, err error)
			Params: []*model.Argument{
				{Name: "p", IsArray: &model.ArgumentArray{Element: &model.Argument{
					TypeName:  "byte",
					BasicInfo: types.IsInteger | types.IsUnsigned,
				}}},
			},
			Results: []*model.Argument{
				{Name: "n", TypeName: "int", BasicInfo: types.IsInteger},
				{Name: "err", IsNamed: &errorNamedArgument},
			},
		},

		{
			Name:    "MethodOfSkippedIface", // real method: MethodOfSkippedIface()
			Params:  []*model.Argument{},
			Results: []*model.Argument{},
		},

		{
			Name: "AnonEmbeddedStruct", // real method: AnonEmbeddedStruct(s struct{ Clean })
			Params: []*model.Argument{
				{Name: "s", IsStruct: &model.ArgumentStruct{
					Fields: []*model.ArgumentStructField{
						{Type: &cleanNamedArgument},
					},
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "AnonStruct", // real method: AnonStruct(s struct{ data int })
			Params: []*model.Argument{
				{Name: "s", IsStruct: &model.ArgumentStruct{
					Fields: []*model.ArgumentStructField{
						{Name: "data", Type: &model.Argument{TypeName: "int", BasicInfo: types.IsInteger}},
					},
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Basic", // real method: Basic(id int64)
			Params: []*model.Argument{
				{Name: "id", TypeName: "int64", BasicInfo: types.IsInteger},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "BasicPointer", // real method: BasicPointer(id *int64)
			Params: []*model.Argument{
				{Name: "id", IsPointer: &model.ArgumentPointer{Element: &model.Argument{TypeName: "int64", BasicInfo: types.IsInteger}}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ChanMethod", // real method: ChanMethod(ch chan *model.Data)
			Params: []*model.Argument{
				{Name: "ch", IsChan: &model.ArgumentChan{
					Direction: types.SendRecv,
					Element:   &dataNamedArgument,
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ChanMethodDir1", // real method: ChanMethodDir1(ch chan<- *model.Data)
			Params: []*model.Argument{
				{Name: "ch", IsChan: &model.ArgumentChan{
					Direction: types.SendOnly,
					Element:   &dataNamedArgument,
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ChanMethodDir2", // real method: ChanMethodDir2(ch <-chan *model.Data)
			Params: []*model.Argument{
				{Name: "ch", IsChan: &model.ArgumentChan{
					Direction: types.RecvOnly,
					Element:   &dataNamedArgument,
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ChanMethodWithChan", // real method: ChanMethodWithChan(ch chan (<-chan *model.Data))
			Params: []*model.Argument{
				{Name: "ch", IsChan: &model.ArgumentChan{
					Direction: types.SendRecv,
					Element: &model.Argument{IsChan: &model.ArgumentChan{
						Direction: types.RecvOnly,
						Element:   &dataNamedArgument,
					}},
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ChanNamedMethod", // real method: ChanNamedMethod(ch NamedChan)
			Params: []*model.Argument{
				{
					Name: "ch",
					IsNamed: &model.Argument{
						IsChan:   &model.ArgumentChan{},
						Package:  packageTestdata,
						TypeName: "NamedChan",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name:    "Clean", // real method: Clean(Clean)
			Params:  []*model.Argument{&cleanNamedArgument},
			Results: []*model.Argument{},
		},

		{
			Name: "CleanAll", // real method: CleanAll([]Clean)
			Params: []*model.Argument{
				{IsArray: &model.ArgumentArray{Element: &cleanNamedArgument}},
			},
			Results: []*model.Argument{},
		},

		{
			Name:       "CleanAny", // real method: CleanAny(...Clean)
			IsVariadic: true,
			Params: []*model.Argument{
				{IsArray: &model.ArgumentArray{IsVariadic: true, Element: &cleanNamedArgument}},
			},
			Results: []*model.Argument{},
		},

		{
			Name:       "CleanAnyAll", // real method: CleanAnyAll(...[]Clean)
			IsVariadic: true,
			Params: []*model.Argument{
				{IsArray: &model.ArgumentArray{
					IsVariadic: true,
					Element:    &model.Argument{IsArray: &model.ArgumentArray{Element: &cleanNamedArgument}},
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "CleanFull", // real method: CleanFull([10]Clean)
			Params: []*model.Argument{
				{IsArray: &model.ArgumentArray{Size: 10, Element: &cleanNamedArgument}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "CleanNamed", // real method: CleanNamed(NamedSlice)
			Params: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsArray:  &model.ArgumentArray{},
						Package:  packageTestdata,
						TypeName: "NamedSlice",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "CleanNamedFull", // real method: CleanNamedFull(NamedArray)
			Params: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsArray:  &model.ArgumentArray{},
						Package:  packageTestdata,
						TypeName: "NamedArray",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "ContextMethod", // real method: ContextMethod(ctx context.Context)
			Params: []*model.Argument{
				{
					Name: "ctx",
					IsNamed: &model.Argument{
						IsInterface: &model.ArgumentInterface{
							Methods: []*model.Method{},
						},
						Package:  "context",
						TypeName: "Context",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Func", // real method: Func(fn func(id int, data *model.Data, variadic ...string) (i int64, err error)) error
			Params: []*model.Argument{
				{
					Name: "fn",
					IsFunc: &model.Method{
						IsVariadic: true,
						Params: []*model.Argument{
							{Name: "id", TypeName: "int", BasicInfo: types.IsInteger},
							{Name: "data", IsPointer: dataNamedArgument.IsPointer},
							{Name: "variadic", IsArray: &model.ArgumentArray{IsVariadic: true, Element: &model.Argument{TypeName: "string", BasicInfo: types.IsString}}},
						},
						Results: []*model.Argument{
							{Name: "i", TypeName: "int64", BasicInfo: types.IsInteger},
							{Name: "err", IsNamed: &errorNamedArgument},
						},
					},
				},
			},
			Results: []*model.Argument{
				{IsNamed: &errorNamedArgument},
			},
		},

		{
			Name: "FuncNamed", // real method: FuncNamed(fn NamedFunc)
			Params: []*model.Argument{
				{
					Name: "fn",
					IsNamed: &model.Argument{
						IsFunc:   &model.Method{},
						Package:  packageTestdata,
						TypeName: "NamedFunc",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "HiddenType", // real method: HiddenType(a1, a2 int64)
			Params: []*model.Argument{
				{Name: "a1", TypeName: "int64", BasicInfo: types.IsInteger},
				{Name: "a2", TypeName: "int64", BasicInfo: types.IsInteger},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "InterfaceAnon", // real method: InterfaceAnon(i interface{ Method(i int) error })
			Params: []*model.Argument{
				{
					Name: "i",
					IsInterface: &model.ArgumentInterface{Methods: []*model.Method{
						{
							Name:    "Method",
							Params:  []*model.Argument{{Name: "i", TypeName: "int", BasicInfo: types.IsInteger}},
							Results: []*model.Argument{{IsNamed: &errorNamedArgument}},
						},
					}},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "InterfaceEmpty", // real method: InterfaceEmpty(i interface{})
			Params: []*model.Argument{
				{Name: "i", IsInterface: &model.ArgumentInterface{Methods: []*model.Method{}}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "InterfaceSkipped", // real method: InterfaceSkipped(skip.Skipped)
			Params: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsInterface: &model.ArgumentInterface{Methods: []*model.Method{}},
						Package:     packageTestdata + "/skipdir",
						TypeName:    "Skipped",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "InterfaceStd", // real method: InterfaceStd(io.Closer)
			Params: []*model.Argument{
				{IsNamed: &model.Argument{
					IsInterface: &model.ArgumentInterface{Methods: []*model.Method{}},
					Package:     "io",
					TypeName:    "Closer",
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Maps", // real method: Maps(map[int]*model.Data) map[*Clean]map[int][]*model.Data
			Params: []*model.Argument{
				{IsMap: &model.ArgumentMap{
					Key:   &model.Argument{TypeName: "int", BasicInfo: types.IsInteger},
					Value: &dataNamedArgument,
				}},
			},
			Results: []*model.Argument{
				{IsMap: &model.ArgumentMap{
					Key: &cleanNamedArgument,
					Value: &model.Argument{IsMap: &model.ArgumentMap{
						Key: &model.Argument{TypeName: "int", BasicInfo: types.IsInteger},
						Value: &model.Argument{IsArray: &model.ArgumentArray{
							Element: &dataNamedArgument,
						}},
					}},
				}},
			},
		},

		{
			Name: "MapsNamed", // real method: MapsNamed(NamedMap) NamedMap
			Params: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsMap:    &model.ArgumentMap{},
						Package:  packageTestdata,
						TypeName: "NamedMap",
					},
				},
			},
			Results: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsMap:    &model.ArgumentMap{},
						Package:  packageTestdata,
						TypeName: "NamedMap",
					},
				},
			},
		},

		{
			Name: "NamedArgs", // real method: NamedArgs(id int64) (m model.Data, err error)
			Params: []*model.Argument{
				{Name: "id", TypeName: "int64", BasicInfo: types.IsInteger},
			},
			Results: []*model.Argument{
				{Name: "m", IsPointer: dataNamedArgument.IsPointer},
				{Name: "err", IsNamed: &errorNamedArgument},
			},
		},

		{
			Name: "NamedIntArg", // real method: NamedIntArg(id NamedInt)
			Params: []*model.Argument{
				{Name: "id", IsNamed: &model.Argument{
					Package:   packageTestdata,
					TypeName:  "NamedInt",
					BasicInfo: types.IsInteger,
				}},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Pointer", // real method: Pointer(*model.Data) error
			Params: []*model.Argument{
				&dataNamedArgument,
			},
			Results: []*model.Argument{
				{IsNamed: &errorNamedArgument},
			},
		},

		{
			Name: "PointerNamed", // real method: PointerNamed(NamedPointer) error
			Params: []*model.Argument{
				{
					IsNamed: &model.Argument{
						IsPointer: &model.ArgumentPointer{},
						Package:   packageTestdata,
						TypeName:  "NamedPointer",
					},
				},
			},
			Results: []*model.Argument{},
		},

		{
			Name: "Unsafe", // real method: Unsafe(u uintptr)
			Params: []*model.Argument{
				{Name: "u", TypeName: "uintptr", BasicInfo: types.IsInteger | types.IsUnsigned},
			},
			Results: []*model.Argument{},
		},
	}
)

func TestAnalyzer(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Search{
		Name:      "AllVariants",
		Root:      "../../testdata/.",
		Recursive: true,
		SkipPaths: []string{"../../testdata/skipdir", "../../testdata/brokencode"},
	}

	buf, log := getLogger()

	res, err := analyzer.New(log, conf).Run(context.Background())
	so.NoError(err)
	so.Len(res, 1)

	iface := res[0]

	so.Equal(packageTestdata, iface.ImportPath)
	so.Equal("testdata", iface.PackageName)
	so.Equal("AllVariants", iface.Name)
	so.Truef(strings.HasSuffix(iface.DirPath, iface.ImportPath), "DirPath %q, ImportPath %q", iface.DirPath, iface.ImportPath)
	so.Truef(filepath.IsAbs(iface.DirPath), "dirPath: %s", iface.DirPath)

	t.Logf("absolute path: %s", iface.DirPath)

	so.Len(iface.Methods, len(expectedMethods))

	t.Logf("%s\n", buf.String())

	so.Len(expectedMethods, len(iface.Methods))

	for i := range iface.Methods {
		so.Equalf(expectedMethods[i], iface.Methods[i], "method (%q != %q) #%d", expectedMethods[i].Name, iface.Methods[i].Name, i+1)
	}
}

// TestAnalyzerErrors just for coverage, checks errors.
func TestAnalyzerErrors(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Search{
		Name:      `\p`,
		Root:      "/fake_dir/not-exists",
		Recursive: false,
		SkipPaths: nil,
	}

	_, log := getLogger()
	ctx := context.Background()
	a := analyzer.New(log, conf)

	_, err := a.Run(ctx)
	targetSyntax := new(syntax.Error)
	so.ErrorAs(err, &targetSyntax)

	conf.Name = ".*"
	_, err = a.Run(ctx)
	targetFS := new(fs.PathError)
	so.ErrorAs(err, &targetFS)

	// no error without go files in code
	conf.Root = "../../testdata/brokencode"
	res, err := a.Run(ctx)
	so.NoError(err)
	so.Len(res, 0)

	// interfaces without methods
	conf.Root = "../../testdata/pkgtest"
	res, err = a.Run(ctx)
	so.NoError(err)
	so.Len(res, 0)

	// interfaces from test packages
	conf.IncludeTests = true
	res, err = a.Run(ctx)
	so.NoError(err)
	so.Len(res, 1)
	so.Equal(res[0].Name, "PkgTestInterface")

	// bad syntax package
	conf.Root = "../../testdata/brokencode/badsyntax"
	_, err = a.Run(ctx)
	so.ErrorIs(err, analyzer.ErrGoSyntax)
	t.Logf("bad syntax error:\n%s", err)

	// canceled context
	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(ctx)
	cancel()

	conf.Root = "../../testdata"
	_, err = a.Run(ctx)
	so.ErrorIs(err, context.Canceled)
}

// TestAnalyzerInculdesNotTypes analyzer should parse only types without variables.
func TestAnalyzerInculdesNotTypes(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Search{
		Name:            "ErrVarIsInterface",
		Root:            "../../testdata/.",
		IncludeNotTypes: false,
	}

	_, log := getLogger()
	ctx := context.Background()

	a := analyzer.New(log, conf)

	res, err := a.Run(ctx)
	so.NoError(err)
	so.Len(res, 0)

	conf.IncludeNotTypes = true

	res, err = analyzer.New(log, conf).Run(context.Background())
	so.NoError(err)
	so.Len(res, 1)

	so.Equal("ErrVarIsInterface", res[0].Name)
}

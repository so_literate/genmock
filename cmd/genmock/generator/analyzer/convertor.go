package analyzer

import (
	"fmt"
	"go/types"
	"regexp"

	"github.com/rs/zerolog"
	"golang.org/x/tools/go/packages"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

type convertor struct {
	log zerolog.Logger

	ifaceNames      *regexp.Regexp
	includeNotTypes bool
}

//nolint:gocognit,gocyclo // it's sad but ok.
func (c *convertor) convertArgument(name string, arg types.Type, isVariadic, isNamed bool) *model.Argument {
	res := &model.Argument{
		Name: name,
	}

	switch tp := arg.(type) {
	case *types.Basic: // Latest level of the argument.
		res.TypeName = tp.Name()
		res.BasicInfo = tp.Info()

	case *types.Named:
		res.IsNamed = c.convertArgument("", tp.Underlying(), false, true)

		if pkg := tp.Obj().Pkg(); pkg != nil {
			res.IsNamed.Package = pkg.Path()
		}

		res.IsNamed.TypeName = tp.Obj().Name()

	case *types.Pointer:
		if isNamed {
			res.IsPointer = &model.ArgumentPointer{}
			break
		}

		res.IsPointer = &model.ArgumentPointer{
			Element: c.convertArgument("", tp.Elem(), false, false),
		}

	case *types.Slice:
		if isNamed {
			res.IsArray = &model.ArgumentArray{}
			break
		}

		res.IsArray = &model.ArgumentArray{
			IsVariadic: isVariadic,
			Size:       0,
			Element:    c.convertArgument("", tp.Elem(), false, false),
		}

	case *types.Array:
		if isNamed {
			res.IsArray = &model.ArgumentArray{}
			break
		}

		res.IsArray = &model.ArgumentArray{
			Size:    tp.Len(),
			Element: c.convertArgument("", tp.Elem(), false, false),
		}

	case *types.Map:
		if isNamed {
			res.IsMap = &model.ArgumentMap{}
			break
		}

		res.IsMap = &model.ArgumentMap{
			Key:   c.convertArgument("", tp.Key(), false, false),
			Value: c.convertArgument("", tp.Elem(), false, false),
		}

	case *types.Signature:
		if isNamed {
			res.IsFunc = &model.Method{}
			break
		}

		res.IsFunc = c.convertMethod("", tp)

	case *types.Chan:
		if isNamed {
			res.IsChan = &model.ArgumentChan{}
			break
		}

		res.IsChan = &model.ArgumentChan{
			Direction: tp.Dir(),
			Element:   c.convertArgument("", tp.Elem(), false, false),
		}

	case *types.Struct:
		if isNamed {
			res.IsStruct = &model.ArgumentStruct{Fields: []*model.ArgumentStructField{}}
			break
		}

		res.IsStruct = &model.ArgumentStruct{
			Fields: make([]*model.ArgumentStructField, tp.NumFields()),
		}

		for i := 0; i < tp.NumFields(); i++ {
			f := tp.Field(i)

			field := &model.ArgumentStructField{
				Tag:  tp.Tag(i),
				Type: c.convertArgument("", f.Type(), false, false),
			}

			if !f.Embedded() {
				field.Name = f.Name()
			}

			res.IsStruct.Fields[i] = field
		}

	case *types.Interface:
		// We can get recursive here, in Context for example, need to check named interface.
		if isNamed {
			// Just interface don't care about methods.
			res.IsInterface = &model.ArgumentInterface{Methods: []*model.Method{}}
			break
		}

		// Have to parse all methods of the anon interface.
		res.IsInterface = &model.ArgumentInterface{Methods: c.convertInterfaceMethods(tp)}

	default:
		panic(fmt.Errorf("%w: unexpected type of the argument %T", ErrUnexpectedData, arg))
	}

	return res
}

func (c *convertor) convertMethod(name string, sig *types.Signature) *model.Method {
	res := &model.Method{
		Name:       name,
		IsVariadic: sig.Variadic(),
		Params:     make([]*model.Argument, sig.Params().Len()),
		Results:    make([]*model.Argument, sig.Results().Len()),
	}

	for i := 0; i < sig.Params().Len(); i++ {
		// variadic is the last param of the signature with Variadic flag.
		isVariadic := sig.Variadic() && i == sig.Params().Len()-1

		arg := sig.Params().At(i)
		res.Params[i] = c.convertArgument(arg.Name(), arg.Type(), isVariadic, false)
	}

	for i := 0; i < sig.Results().Len(); i++ {
		arg := sig.Results().At(i)
		res.Results[i] = c.convertArgument(arg.Name(), arg.Type(), false, false)
	}

	return res
}

func (c *convertor) convertInterfaceMethods(tp types.Type) []*model.Method {
	iface := tp.(*types.Interface) //nolint:errcheck,forcetypeassert // without error returning there.

	if iface.Empty() {
		return []*model.Method{}
	}

	res := make([]*model.Method, 0, iface.NumMethods())

	for i := 0; i < iface.NumEmbeddeds(); i++ {
		res = append(res, c.convertInterfaceMethods(iface.EmbeddedType(i).Underlying())...)
	}

	for i := 0; i < iface.NumExplicitMethods(); i++ {
		m := iface.ExplicitMethod(i)
		sig := m.Type().(*types.Signature) //nolint:errcheck,forcetypeassert // without error returning there.

		res = append(res, c.convertMethod(m.Name(), sig))
	}

	return res
}

func (c *convertor) run(pkg *packages.Package, dirPath string) []*model.Interface {
	// large debug printer here :) pkg.Types.Scope().WriteTo(os.Stdout, 0, true).

	c.log = c.log.With().Str("package", pkg.PkgPath).Logger()

	res := make([]*model.Interface, 0)

	for _, name := range pkg.Types.Scope().Names() {
		obj := pkg.Types.Scope().Lookup(name)

		if !types.IsInterface(obj.Type()) {
			continue
		}

		_, isTypeStatement := obj.(*types.TypeName)

		if !c.includeNotTypes && !isTypeStatement {
			c.log.Debug().Msgf("skipped not a type statement: %q", name)
			continue
		}

		if !c.ifaceNames.MatchString(name) {
			c.log.Debug().Msgf("skipped by the interface name: %q", name)
			continue
		}

		c.log.Debug().Msgf("convert interface: %s", name)

		methods := c.convertInterfaceMethods(obj.Type().Underlying())

		if len(methods) == 0 { // Skips empty interfaces.
			continue
		}

		iface := &model.Interface{
			Name:        name,
			PackageName: pkg.Name,
			ImportPath:  pkg.PkgPath,
			DirPath:     dirPath,
			Methods:     methods,
		}

		res = append(res, iface)
	}

	return res
}

// Package analyzer contains methods to analyze go file to find interfaces.
package analyzer

import (
	"context"
	"errors"
	"fmt"
	"go/token"
	"io/fs"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/rs/zerolog"
	"golang.org/x/tools/go/packages"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

var (
	// ErrGoSyntax returns when parsed packages contains errors.
	ErrGoSyntax = errors.New("go syntax")
	// ErrUnexpectedData returns when inspector or analyzer got unexpected data, description in error.
	ErrUnexpectedData = errors.New("unexpected data")
	// ErrWrongScopeType returns when inspector got scope of the another type not the Interface.
	// This is not a error when you pass not filtered scopes.
	ErrWrongScopeType = errors.New("wrong type of the scope")
)

// Analyzer is a searcher and analyzer go files.
type Analyzer struct {
	log        zerolog.Logger
	conf       *config.Search
	ifaceNames *regexp.Regexp
	skipPaths  map[string]struct{}

	fset *token.FileSet
}

// New returns new analyzer with validated config.
func New(log *zerolog.Logger, conf *config.Search) *Analyzer {
	return &Analyzer{
		log:  log.With().Bool("analyzer", true).Logger(),
		conf: conf,
	}
}

func (a *Analyzer) collectDirs(ctx context.Context) ([]string, error) {
	res := make([]string, 0, 10)

	walker := func(path string, info fs.DirEntry, err error) error {
		select {
		case <-ctx.Done():
			return fmt.Errorf("context error: %w", ctx.Err())
		default:
		}

		if err != nil {
			return fmt.Errorf("walker: %w", err)
		}

		if !info.IsDir() {
			return nil
		}

		// Skips all subdir if config without recursive search.
		if path != a.conf.Root && !a.conf.Recursive {
			a.log.Debug().Msgf("(not recursive) skip sub dir: %q", path)
			return filepath.SkipDir
		}

		// Checks dir in skip list.
		if _, ok := a.skipPaths[path]; ok {
			a.log.Debug().Msgf("skip path: %q", path)
			return filepath.SkipDir
		}

		path, err = filepath.Abs(path)
		if err != nil {
			return fmt.Errorf("filepath.Abs %q: %w", path, err)
		}

		res = append(res, path)

		return nil
	}

	err := filepath.WalkDir(a.conf.Root, walker)
	if err != nil {
		return nil, fmt.Errorf("filepath.Walk: %w", err)
	}

	return res, nil
}

func (a *Analyzer) parsePackage(ctx context.Context, path string) ([]*model.Interface, error) {
	log := a.log.With().Str("path", path).Logger()

	log.Debug().Msg("start parsing package")

	cfg := &packages.Config{
		Mode:    packages.NeedName | packages.NeedFiles | packages.NeedTypes | packages.NeedTypesInfo,
		Context: ctx,
		Dir:     path,
		Tests:   a.conf.IncludeTests,
		Fset:    a.fset,
	}

	pkgs, err := packages.Load(cfg)
	if err != nil {
		return nil, fmt.Errorf("packages.Load: %w", err)
	}

	var res []*model.Interface

	for _, pkg := range pkgs {
		select {
		case <-ctx.Done():
			return nil, fmt.Errorf("context error: %w", ctx.Err())
		default:
		}

		if len(pkg.GoFiles) == 0 {
			log.Debug().Msg("skip package without go files")
			continue
		}

		if len(pkg.Errors) != 0 {
			texts := make([]string, len(pkg.Errors))
			for i, pkgErr := range pkg.Errors {
				texts[i] = fmt.Sprintf(" - %s", pkgErr.Error())
			}

			return nil, fmt.Errorf("%w:\n%s", ErrGoSyntax, strings.Join(texts, "\n"))
		}

		c := &convertor{
			log:             log,
			ifaceNames:      a.ifaceNames,
			includeNotTypes: a.conf.IncludeNotTypes,
		}

		ifaces := c.run(pkg, path)

		log.Debug().Msgf("parsed interfaces: %d", len(ifaces))

		res = append(res, ifaces...)
	}

	return res, nil
}

// Run runs file analyze using seted config params.
// Collects files to analyze and parses them to collect interface types.
func (a *Analyzer) Run(ctx context.Context) ([]*model.Interface, error) {
	a.log.Debug().
		Interface("conf", *a.conf).
		Msg("running analyzer")

	var err error
	a.ifaceNames, err = regexp.Compile(a.conf.Name)
	if err != nil {
		return nil, fmt.Errorf("regexp.Compile conf.Name %q: %w", a.conf.Name, err)
	}

	a.skipPaths = make(map[string]struct{}, len(a.conf.SkipPaths))
	for _, dir := range a.conf.SkipPaths {
		a.skipPaths[dir] = struct{}{}
	}

	toVisit, err := a.collectDirs(ctx)
	if err != nil {
		return nil, fmt.Errorf("collectDirs: %w", err)
	}

	a.log.Debug().Strs("toVisit", toVisit).Msg("collected paths to visit")

	res := make([]*model.Interface, 0, len(toVisit))

	a.fset = token.NewFileSet()

	for _, path := range toVisit {
		ifaces, err := a.parsePackage(ctx, path)
		if err != nil {
			return nil, fmt.Errorf("failed to parse package %q: %w", path, err)
		}

		res = append(res, ifaces...)
	}

	return res, nil
}

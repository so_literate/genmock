// Package model contains models for transmission between the analyzer and the printer.
package model

import (
	"go/types"
)

// ArgumentPointer represents an pointer type argument.
type ArgumentPointer struct {
	Element *Argument // Argument of the pointer, can be simpe struct or slice and etc.
}

// ArgumentArray represents array, slice or variadic argument.
type ArgumentArray struct {
	IsVariadic bool      // Have 3 dots ... in type name, prints without breakets.
	Size       int64     // 0 is a slice.
	Element    *Argument // Array element.
}

// ArgumentMap represents map type argument.
type ArgumentMap struct {
	Key   *Argument
	Value *Argument
}

// ArgumentChan represents channel type argument.
type ArgumentChan struct {
	Direction types.ChanDir // Channel direction: 'chan', '<-chan', 'chan<-'.
	Element   *Argument     // Passed element of the channel.
}

// ArgumentStructField represents field of the stuct.
type ArgumentStructField struct {
	Name string    // Name of the field, can be empty.
	Tag  string    // Tag of the field, can be empty.
	Type *Argument // Type of the field, can be with package.
}

// ArgumentStruct represents anonymous struct as argument.
type ArgumentStruct struct {
	Fields []*ArgumentStructField // Struct's field list.
}

// ArgumentInterface represents anonymous interface as argument.
type ArgumentInterface struct {
	Methods []*Method // Methods list of the interface.
}

// Argument of the method inerface.
type Argument struct {
	Name string // Name of the argument.

	IsPointer   *ArgumentPointer   // Argument is a pointer of type, contains another type.
	IsArray     *ArgumentArray     // Argument is a slice of types, element of the slice in this field.
	IsFunc      *Method            // Argument is a func, can contains params and results.
	IsMap       *ArgumentMap       // Argument is a map, contains key and value of the map.
	IsChan      *ArgumentChan      // Argument is a channel, contains direction and types.
	IsStruct    *ArgumentStruct    // Argument is a anon struct, contains fields.
	IsInterface *ArgumentInterface // Argument is a anon inerface, contains methods.
	IsNamed     *Argument          // Argument is a named type.

	Package   string          // Package path of the type.
	TypeName  string          // Type name without package prefix (int, byte, Data).
	BasicInfo types.BasicInfo // Basic info of the type.
}

// Method describes mehtod of the interface.
type Method struct {
	Name       string      // Name of the method.
	IsVariadic bool        // Params of the method have a variadic argument.
	Params     []*Argument // Input arguments of the methods.
	Results    []*Argument // Result arguments if the methods.
}

// Interface describes the interface found by the analyzer.
type Interface struct {
	Name        string    // Name of the interface.
	PackageName string    // Name of the package with interface.
	ImportPath  string    // Import path to the package with interface.
	DirPath     string    // Absolute path to directory with interface.
	Methods     []*Method // Methods list of the interface.
}

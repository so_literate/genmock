package printer

import (
	"fmt"
	"strings"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

const setterReceiverName = "_s"

func getSetterTypeName(ifaceName, methodName string) string {
	return fmt.Sprintf("%s_MockSet_%s", ifaceName, methodName)
}

func (p *Printer) getExpectedValueParam(arg *model.Argument) string {
	if isPossibleConvertNil(arg) {
		return fmt.Sprintf(`%s(%q)`,
			p.convertor.GetQualifiedType(pkgGenmock, "AnythingOfType"),
			p.convertor.ArgumentTypeConvertString(arg),
		)
	}

	return p.convertor.GetQualifiedType(pkgGenmock, "Anything")
}

func (p *Printer) getExpectedValueParams(args []*model.Argument) string {
	if len(args) == 0 {
		return ""
	}

	values := make([]string, len(args))
	for i, arg := range args {
		values[i] = p.getExpectedValueParam(arg)
	}

	return strings.Join(values, ", ")
}

func (p *Printer) printSetterMethod(w *writer, ifaceName string, method *model.Method) {
	setter := getSetterTypeName(ifaceName, method.Name)

	w.printf(`// %s allows to set arguments and results of the mock call %s.`, setter, method.Name)
	w.printf(`type %s struct {`, setter)
	w.printf(`	Call *%s`, p.convertor.GetQualifiedType(pkgGenmock, "Call"))
	w.printf(`}`)
	w.printf(``)

	w.printf(`// On_%s adds a call of the Method to mock.`, method.Name)
	w.printf(`func (%s *%s) On_%s() %s {`, implReceiverName, getImplementerTypeName(ifaceName), method.Name, setter)
	w.printf(`	call := %s(`, p.convertor.GetQualifiedType(pkgGenmock, "NewCall"))
	w.printf(`		"%s",`, method.Name)
	w.printf(`		[]interface{}{%s},`, p.getExpectedValueParams(method.Params))
	w.printf(`		[]interface{}{%s},`, p.getZeroValues(method.Results))
	w.printf(`		1,`)
	w.printf(`	)`)
	w.printf(`	%s.Mock.AddCall(call)`, implReceiverName)
	w.printf(`	return %s{Call: call}`, setter)
	w.printf(`}`)
	w.printf(``)

	if l := len(method.Params); l != 0 {
		p.printSetterMethodArgs(w, setter, method.Params)

		for i, arg := range method.Params {
			p.printSetterMethodArg(w, setter, i, l == 1, arg)
		}
	}

	if lr := len(method.Results); lr != 0 {
		p.printSetterMethodRets(w, setter, method.Results)

		if lr != 1 {
			for i, arg := range method.Results {
				p.printSetterMethodRet(w, setter, i, arg)
			}
		}
	}

	p.printSetterMethodTimes(w, setter)
}

func (p *Printer) printSetterMethodArgs(w *writer, setter string, args []*model.Argument) {
	w.printf(`// Args sets the exact values of the arguments.`)
	w.printf(`func (%s %s) Args%s %s {`,
		setterReceiverName, setter, p.convertor.MethodTuplesString(&model.Method{Params: args}), setter,
	)

	for i, arg := range args {
		w.printf(`	%s.Call.Args[%d] = %s`, setterReceiverName, i, arg.Name)
	}

	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)

	m := &model.Method{Params: make([]*model.Argument, len(args))}
	for i, arg := range args {
		m.Params[i] = &model.Argument{Name: arg.Name, IsInterface: &model.ArgumentInterface{}}
	}

	w.printf(`// ArgsAnything sets the interface values of the arguments.`)
	w.printf(`func (%s %s) ArgsAnything%s %s {`,
		setterReceiverName, setter, p.convertor.MethodTuplesString(m), setter,
	)

	for i, arg := range args {
		w.printf(`	%s.Call.Args[%d] = %s`, setterReceiverName, i, arg.Name)
	}

	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)
}

func (p *Printer) printSetterMethodArg(w *writer, setter string, i int, singleArg bool, arg *model.Argument) {
	argMethodName := getArgMethodName(arg.Name)

	if !singleArg {
		// Exact.
		w.printf(`// Arg%s sets the exact value of the %s argument.`, argMethodName, arg.Name)
		w.printf(`func (%s %s) Arg%s(%s %s) %s {`,
			setterReceiverName, setter, argMethodName, arg.Name, p.convertor.ArgumentTypeString(arg), setter,
		)
		w.printf(`	%s.Call.Args[%d] = %s`, setterReceiverName, i, arg.Name)
		w.printf(`	return %s`, setterReceiverName)
		w.printf(`}`)
		w.printf(``)

		// Anything.
		w.printf(`// Arg%s_Anything sets the interface value of the %s argument.`, argMethodName, arg.Name)
		w.printf(`func (%s %s) Arg%s_Anything(%s interface{}) %s {`,
			setterReceiverName, setter, argMethodName, arg.Name, setter,
		)
		w.printf(`	%s.Call.Args[%d] = %s`, setterReceiverName, i, arg.Name)
		w.printf(`	return %s`, setterReceiverName)
		w.printf(`}`)
		w.printf(``)
	}

	// Matcher.
	matcher := fmt.Sprintf(`func(%s interface{}) bool`, arg.Name)
	if isPossibleConvertNil(arg) {
		matcher = fmt.Sprintf(`func(%s %s) bool`, arg.Name, p.convertor.ArgumentTypeConvertString(arg))
	}

	w.printf(`// Arg%s_Matcher sets matcher of the %s argument value.`, argMethodName, arg.Name)
	w.printf(`func (%s %s) Arg%s_Matcher(matcher %s) %s {`,
		setterReceiverName, setter, argMethodName, matcher, setter,
	)

	matcherName := "matcher"
	if isPossibleConvertNil(arg) {
		w.printf(`realMatcher := func(arg interface{}) bool {`)
		w.printf(`	value, ok := arg.(%s)`, p.convertor.ArgumentTypeConvertString(arg))
		w.printf(`	if !ok {`)
		w.printf(`		return false`)
		w.printf(`	}`)
		w.printf(`	return matcher(value)`)
		w.printf(`}`)
		w.printf(``)

		matcherName = "realMatcher"
	}

	w.printf(`	%s.Call.Args[%d] = %s`, setterReceiverName, i, matcherName)
	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)
}

func (p *Printer) printSetterMethodRets(w *writer, setter string, args []*model.Argument) {
	w.printf(`// Rets sets the exact values of the result parameters.`)
	w.printf(`func (%s %s) Rets%s %s {`,
		setterReceiverName, setter, p.convertor.MethodTuplesString(&model.Method{Params: args}), setter,
	)

	for i, arg := range args {
		w.printf(`	%s.Call.Returns[%d] = %s`, setterReceiverName, i, arg.Name)
	}

	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)
}

func (p *Printer) printSetterMethodRet(w *writer, setter string, i int, arg *model.Argument) {
	argMethodName := getArgMethodName(arg.Name)

	w.printf(`// Ret%s sets the exact values of the %s result parameter.`, argMethodName, arg.Name)
	w.printf(`func (%s %s) Ret%s(%s %s) %s {`,
		setterReceiverName, setter, argMethodName, arg.Name, p.convertor.ArgumentTypeString(arg), setter,
	)
	w.printf(`	%s.Call.Returns[%d] = %s`, setterReceiverName, i, arg.Name)
	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)
}

func (p *Printer) printSetterMethodTimes(w *writer, setter string) {
	w.printf(`// Times sets number of times to call this caller of the method.`)
	w.printf(`func (%s %s) Times(times int) %s {`, setterReceiverName, setter, setter)
	w.printf(`	%s.Call.Times = times`, setterReceiverName)
	w.printf(`	return %s`, setterReceiverName)
	w.printf(`}`)
	w.printf(``)
}

package printer_test

import (
	"fmt"
	"go/types"
	"testing"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/printer"
)

const (
	pkg       = "account/repo/project"
	fooBarPkg = "foo/bar"
)

func TestConvertorImports(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCasePkgName := func(in, expectedPkgName string) {
		realPkgName := c.GetPkgPathName(in)
		if realPkgName != expectedPkgName {
			t.Fatalf("unexpeted pkg name: in %q, want %q, got %q", in, expectedPkgName, realPkgName)
		}
	}

	testCasePkgName("", "")
	testCasePkgName(pkg, "")
	testCasePkgName("fmt", "fmt")
	testCasePkgName(fmt.Sprintf("%s/fmt", pkg), "fmt2")

	testCasePkgName("gitlab.com/so_literate/genmock/cmd/genmock/generator/printer", "printer")
	testCasePkgName("gitlab.com/so_literate/genmock/cmd/genmock/generator/printer", "printer")
	testCasePkgName("office/printer", "printer2")

	expectedImports := `	fmt2 "account/repo/project/fmt"
	"fmt"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/printer"
	printer2 "office/printer"`

	imports := c.PrintFileImports()

	if imports != expectedImports {
		t.Fatalf("unexpected imports: %s", imports)
	}

	c.SetFileParams("new/pkg")
	testCasePkgName("new/pkg", "")
	testCasePkgName("office/printer", "printer2")
	testCasePkgName("public/printer", "printer3")

	expectedImports = `	printer2 "office/printer"
	printer3 "public/printer"`

	imports = c.PrintFileImports()

	if imports != expectedImports {
		t.Fatalf("unexpected imports: %s", imports)
	}

	c.SetFileParams(pkg)

	testCaseTypeName := func(path, typeName, expectedTypeName string) {
		realTypeName := c.GetQualifiedType(path, typeName)
		if realTypeName != expectedTypeName {
			t.Fatalf("unexpeted type name: path %q, typeName %q: want %q, got %q", path, typeName, expectedTypeName, realTypeName)
		}
	}

	testCaseTypeName("office/printer", "Data", "printer2.Data")
	testCaseTypeName(pkg, "Data", "Data")
	testCaseTypeName("", "int", "int")
}

var (
	fooBarDataNamed = &model.Argument{
		IsNamed: &model.Argument{
			IsStruct: &model.ArgumentStruct{},
			Package:  fooBarPkg,
			TypeName: "Data",
		},
	}

	errorNamed = &model.Argument{
		IsNamed: &model.Argument{
			TypeName:    "error",
			IsInterface: &model.ArgumentInterface{},
		},
	}
)

func TestArgumentString(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCase := func(a *model.Argument, expected string) {
		t.Helper()

		res := c.ArgumentString(a)
		if res != expected {
			t.Fatalf("unexpected result: want %q, got %q", expected, res)
		}
	}

	//
	// Basic types:
	//
	a := &model.Argument{
		Name:     "arg",
		TypeName: "int",
	}

	testCase(a, "arg int")

	//
	// Named types:
	//
	a.TypeName = ""
	a.IsNamed = fooBarDataNamed.IsNamed

	testCase(a, "arg bar.Data")

	//
	// Pointers
	//
	a = &model.Argument{
		Name:      "argP",
		IsPointer: &model.ArgumentPointer{Element: fooBarDataNamed},
	}
	testCase(a, "argP *bar.Data")

	//
	// Arrays
	//
	a = &model.Argument{
		Name: "arr",
		IsArray: &model.ArgumentArray{
			Element: &model.Argument{
				IsPointer: &model.ArgumentPointer{Element: fooBarDataNamed},
			},
		},
	}
	testCase(a, "arr []*bar.Data")

	a.IsArray.Size = 10
	testCase(a, "arr [10]*bar.Data")

	a.IsArray.IsVariadic = true
	testCase(a, "arr ...*bar.Data")

	a.IsArray.Element = &model.Argument{
		IsArray: &model.ArgumentArray{
			Size: 0,
			Element: &model.Argument{
				IsPointer: &model.ArgumentPointer{Element: fooBarDataNamed},
			},
		},
	}

	testCase(a, "arr ...[]*bar.Data")

	//
	// Functions
	//
	a = &model.Argument{
		Name:   "fn",
		IsFunc: &model.Method{},
	}
	testCase(a, "fn func()")

	a.IsFunc.Params = []*model.Argument{
		{TypeName: "int"},
		{IsArray: &model.ArgumentArray{IsVariadic: true, Element: fooBarDataNamed}},
	}

	a.IsFunc.Results = []*model.Argument{errorNamed}

	testCase(a, "fn func(int, ...bar.Data) error")

	a.IsFunc.Results = []*model.Argument{
		{Name: "d", Package: fooBarPkg, TypeName: "Data"},
		{Name: "err", IsNamed: errorNamed.IsNamed},
	}

	testCase(a, "fn func(int, ...bar.Data) (d bar.Data, err error)")

	//
	// Maps
	//
	a = &model.Argument{
		Name: "m",
		IsMap: &model.ArgumentMap{
			Key:   &model.Argument{TypeName: "string"},
			Value: &model.Argument{IsArray: &model.ArgumentArray{Element: fooBarDataNamed}},
		},
	}
	testCase(a, "m map[string][]bar.Data")

	a.IsMap.Value = &model.Argument{IsMap: &model.ArgumentMap{
		Key:   fooBarDataNamed,
		Value: &model.Argument{IsArray: &model.ArgumentArray{Element: fooBarDataNamed}},
	}}
	testCase(a, "m map[string]map[bar.Data][]bar.Data")

	//
	// Channels
	//
	a = &model.Argument{
		Name: "ch",
		IsChan: &model.ArgumentChan{
			Direction: types.SendRecv,
			Element:   fooBarDataNamed,
		},
	}
	testCase(a, "ch chan bar.Data")

	a.IsChan.Direction = types.SendOnly
	testCase(a, "ch chan<- bar.Data")

	a.IsChan.Direction = types.RecvOnly
	testCase(a, "ch <-chan bar.Data")

	a.IsChan.Direction = types.SendRecv
	a.IsChan.Element = &model.Argument{
		IsChan: &model.ArgumentChan{
			Direction: types.RecvOnly,
			Element:   fooBarDataNamed,
		},
	}
	testCase(a, "ch chan (<-chan bar.Data)")

	//
	// Structs
	//
	a = &model.Argument{
		Name: "s",
		IsStruct: &model.ArgumentStruct{
			Fields: []*model.ArgumentStructField{
				{Name: "", Type: &model.Argument{Package: fooBarPkg, TypeName: "Data"}}, // embedded
				{Name: "data", Type: &model.Argument{TypeName: "int64"}},                // with field
				{Name: "Tagged", Type: &model.Argument{TypeName: "string"}, Tag: `json:"tagged"`},
			},
		},
	}
	testCase(a, "s struct{bar.Data; data int64; Tagged string \"json:\\\"tagged\\\"\"}")

	//
	// Interfaces
	//
	a = &model.Argument{Name: "i", IsInterface: &model.ArgumentInterface{}}
	testCase(a, "i interface {}")

	a.IsInterface.Methods = []*model.Method{
		{Name: "Method", Params: []*model.Argument{{Name: "d", IsNamed: fooBarDataNamed.IsNamed}}},
		{Name: "Second", Params: []*model.Argument{{Name: "x", TypeName: "int64"}}, Results: []*model.Argument{errorNamed}},
	}
	testCase(a, "i interface { Method(d bar.Data); Second(x int64) error}")
}

func TestArgumentTypeString(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCase := func(a *model.Argument, expected string) {
		res := c.ArgumentTypeString(a)
		if res != expected {
			t.Fatalf("unexpected result: want %q, got %q", expected, res)
		}
	}

	a := &model.Argument{
		Name:     "arg",
		TypeName: "int",
	}

	testCase(a, "int")
}

func TestMethodTuplesString(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCase := func(a *model.Method, expected string) {
		res := c.MethodTuplesString(a)
		if res != expected {
			t.Fatalf("unexpected result: want %q, got %q", expected, res)
		}
	}

	method := &model.Method{
		Name:    "Method",
		Params:  []*model.Argument{{Name: "x", TypeName: "int64"}},
		Results: []*model.Argument{{Name: "res", TypeName: "string"}, {Name: "err", IsNamed: errorNamed.IsNamed}},
	}

	testCase(method, "(x int64) (res string, err error)")

	method.Results = []*model.Argument{errorNamed}
	testCase(method, "(x int64) error")

	method.Results = []*model.Argument{{Name: "err", IsNamed: errorNamed.IsNamed}}
	testCase(method, "(x int64) (err error)")
}

func TestGetZeroValue(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCase := func(a *model.Argument, expected string) {
		res := c.GetZeroValue(a)
		if res != expected {
			t.Fatalf("unexpected result: want %q, got %q", expected, res)
		}
	}

	// Basic types
	a := &model.Argument{BasicInfo: types.IsBoolean}
	testCase(a, "false")
	a.BasicInfo = types.IsInteger
	testCase(a, "0")
	a.BasicInfo = types.IsFloat
	testCase(a, "0")
	a.BasicInfo = types.IsComplex
	testCase(a, "0")
	a.BasicInfo = types.IsString
	testCase(a, `""`)

	// Anonymous struct.
	a = &model.Argument{IsStruct: &model.ArgumentStruct{Fields: []*model.ArgumentStructField{
		{Name: "Named", Type: fooBarDataNamed},
		{Name: "Basic", Type: &model.Argument{TypeName: "int", BasicInfo: types.IsInteger}},
	}}}
	testCase(a, "struct{Named bar.Data; Basic int}{}")

	// Nilable values.
	a = &model.Argument{IsArray: new(model.ArgumentArray)}
	testCase(a, "nil")

	// Named value.
	a = &model.Argument{IsNamed: &model.Argument{IsArray: new(model.ArgumentArray)}}
	testCase(a, "nil")

	a = &model.Argument{IsNamed: &model.Argument{TypeName: "int", BasicInfo: types.IsInteger}}
	testCase(a, "0")

	a = &model.Argument{IsNamed: &model.Argument{IsStruct: new(model.ArgumentStruct), Package: pkg, TypeName: "local"}}
	testCase(a, "local{}")

	a.IsNamed.Package = fooBarPkg
	a.IsNamed.TypeName = "Data"
	testCase(a, "bar.Data{}")
}

func TestArgumentTypeConvertString(t *testing.T) {
	t.Parallel()

	c := printer.NewConvertor()
	c.SetFileParams(pkg)

	testCase := func(a *model.Argument, expected string) {
		res := c.ArgumentTypeConvertString(a)
		if res != expected {
			t.Fatalf("unexpected result: want %q, got %q", expected, res)
		}
	}

	a := &model.Argument{IsNamed: fooBarDataNamed.IsNamed}
	testCase(a, `bar.Data`)

	a = &model.Argument{IsArray: &model.ArgumentArray{Element: fooBarDataNamed}}
	testCase(a, `[]bar.Data`)

	a.IsArray.IsVariadic = true
	testCase(a, `[]bar.Data`)

	a = &model.Argument{
		IsArray: &model.ArgumentArray{
			IsVariadic: true,
			Element:    &model.Argument{IsArray: &model.ArgumentArray{Element: fooBarDataNamed}},
		},
	}
	testCase(a, `[][]bar.Data`)
}

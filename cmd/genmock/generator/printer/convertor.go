package printer

import (
	"bytes"
	"fmt"
	"go/types"
	"path/filepath"
	"sort"
	"strconv"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

// pathImport contains alias, name and path of the import.
type pathImport struct {
	alias string // May be empty if package with a same base of the path used in first time.
	path  string // Path of the import.
	name  string // Base of the path, may be with counter suffix.
}

// String returns the row of the import block.
func (ui *pathImport) String() string {
	if ui.alias != "" {
		return fmt.Sprintf("	%s %q", ui.alias, ui.path)
	}

	return fmt.Sprintf("	%q", ui.path)
}

// Convertor converts models to code.
type Convertor struct {
	pkg             string
	fileImports     map[string]*pathImport // used imports of this file.
	usedNames       map[string]int         // keeps already used aliases of imports.
	pkgPathToImport map[string]*pathImport // keeps map of the full import path to alias.
}

// NewConvertor returns a new converter based on a pkg name of the generated file.
func NewConvertor() *Convertor {
	return &Convertor{
		pkg:             "", // You have to call SetFileParams to set current package.
		fileImports:     make(map[string]*pathImport, 2),
		usedNames:       make(map[string]int, 2),
		pkgPathToImport: make(map[string]*pathImport, 2),
	}
}

// GetPkgPathName returns name of the path pkg for types.
func (c *Convertor) GetPkgPathName(pkgPath string) string {
	if pkgPath == "" {
		return ""
	}

	if c.pkg == pkgPath { // it's a local package, just use it without import.
		return ""
	}

	ui, ok := c.pkgPathToImport[pkgPath]
	if ok {
		if _, ok = c.fileImports[pkgPath]; !ok {
			c.fileImports[pkgPath] = ui
		}

		return ui.name
	}

	// Have to create a new alias and add this pkg path to import list.
	base := filepath.Base(pkgPath)

	ui = &pathImport{
		path: pkgPath,
		name: base,
	}

	counter := c.usedNames[base]

	counter++

	if counter > 1 { // don't add suffix at name if it is a first usage.
		ui.name = fmt.Sprintf("%s%d", base, counter)
		ui.alias = ui.name
	}

	c.usedNames[base] = counter
	c.pkgPathToImport[pkgPath] = ui
	c.fileImports[pkgPath] = ui

	return ui.name
}

func (c *Convertor) printFileImports(buf *bytes.Buffer) {
	list := make([]*pathImport, len(c.fileImports))

	i := 0
	for _, ui := range c.fileImports {
		list[i] = ui
		i++
	}

	sort.Slice(list, func(i, j int) bool { return list[i].path < list[j].path })

	for i, ui := range list {
		if i > 0 {
			buf.WriteByte('\n')
		}

		buf.WriteString(ui.String())
	}
}

// PrintFileImports prints all imports collected in usedImports.
// Prints just rows with import and alias.
func (c *Convertor) PrintFileImports() string {
	buf := bytes.Buffer{}

	c.printFileImports(&buf)

	return buf.String()
}

// SetFileParams resets used imports of the file and set new package name.
func (c *Convertor) SetFileParams(currentPkg string) {
	c.pkg = currentPkg
	c.fileImports = make(map[string]*pathImport, 2)
}

// GetQualifiedType returns type name with package prefix.
func (c *Convertor) GetQualifiedType(pkgPath, typeName string) string {
	pkg := c.GetPkgPathName(pkgPath)
	if pkg == "" {
		return typeName
	}

	return fmt.Sprintf("%s.%s", pkg, typeName)
}

func (c *Convertor) arrayArgumentFormat(buf *bytes.Buffer, arr *model.ArgumentArray) {
	if arr.IsVariadic { // Variadic is not a real array/slice in printer.
		buf.WriteString("...")
		c.argumentTypeFormat(buf, arr.Element)
		return
	}

	buf.WriteByte('[')

	if arr.Size != 0 {
		buf.WriteString(strconv.FormatInt(arr.Size, 10))
	}

	buf.WriteByte(']')

	c.argumentTypeFormat(buf, arr.Element)
}

func (c *Convertor) methodTuplesFormat(buf *bytes.Buffer, m *model.Method) {
	buf.WriteByte('(')

	for i, p := range m.Params {
		if i > 0 {
			buf.WriteString(", ")
		}

		c.argumentFormat(buf, p)
	}

	buf.WriteByte(')')

	parens := false

	switch l := len(m.Results); {
	case l == 0:
		return
	case l == 1 && m.Results[0].Name != "":
		parens = true
	case l > 1:
		parens = true
	}

	buf.WriteByte(' ')

	if parens {
		buf.WriteByte('(')
	}

	for i, r := range m.Results {
		if i > 0 {
			buf.WriteString(", ")
		}

		c.argumentFormat(buf, r)
	}

	if parens {
		buf.WriteByte(')')
	}
}

func (c *Convertor) chanArgumentFormat(buf *bytes.Buffer, ch *model.ArgumentChan) {
	parens := false

	switch ch.Direction {
	case types.SendRecv:
		buf.WriteString("chan ")
		// chan (<-chan T) requires parentheses.
		if ch.Element.IsChan != nil && ch.Element.IsChan.Direction == types.RecvOnly {
			parens = true
		}

	case types.SendOnly:
		buf.WriteString("chan<- ")

	case types.RecvOnly:
		buf.WriteString("<-chan ")
	}

	if parens {
		buf.WriteByte('(')
	}

	c.argumentTypeFormat(buf, ch.Element)

	if parens {
		buf.WriteByte(')')
	}
}

func (c *Convertor) structArgumentFormat(buf *bytes.Buffer, s *model.ArgumentStruct) {
	buf.WriteString("struct{")

	for i, f := range s.Fields {
		if i > 0 {
			buf.WriteString("; ")
		}

		if f.Name != "" {
			buf.WriteString(f.Name)
			buf.WriteByte(' ')
		}

		c.argumentFormat(buf, f.Type)

		if f.Tag != "" {
			fmt.Fprintf(buf, " %q", f.Tag)
		}
	}

	buf.WriteByte('}')
}

func (c *Convertor) interfaceArgumentFormat(buf *bytes.Buffer, s *model.ArgumentInterface) {
	buf.WriteString("interface {")

	for i, method := range s.Methods {
		if i > 0 {
			buf.WriteString("; ")
		} else {
			buf.WriteByte(' ')
		}

		buf.WriteString(method.Name)
		c.methodTuplesFormat(buf, method)
	}

	buf.WriteByte('}')
}

func (c *Convertor) argumentTypeFormat(buf *bytes.Buffer, a *model.Argument) {
	switch {
	case a.IsPointer != nil:
		buf.WriteByte('*')
		c.argumentFormat(buf, a.IsPointer.Element)

	case a.IsArray != nil:
		c.arrayArgumentFormat(buf, a.IsArray)

	case a.IsFunc != nil:
		buf.WriteString("func")
		c.methodTuplesFormat(buf, a.IsFunc)

	case a.IsMap != nil:
		buf.WriteString("map[")
		c.argumentTypeFormat(buf, a.IsMap.Key)
		buf.WriteByte(']')
		c.argumentTypeFormat(buf, a.IsMap.Value)

	case a.IsChan != nil:
		c.chanArgumentFormat(buf, a.IsChan)

	case a.IsStruct != nil:
		c.structArgumentFormat(buf, a.IsStruct)

	case a.IsInterface != nil:
		c.interfaceArgumentFormat(buf, a.IsInterface)

	case a.IsNamed != nil:
		a = a.IsNamed // Just print named type with package and type.
		fallthrough

	default:
		// Last layer of the argument description with package and type name.
		buf.WriteString(c.GetQualifiedType(a.Package, a.TypeName))
	}
}

// ArgumentTypeString converts method's argument type in string.
func (c *Convertor) ArgumentTypeString(a *model.Argument) string {
	buf := bytes.Buffer{}
	c.argumentTypeFormat(&buf, a)
	return buf.String()
}

func (c *Convertor) argumentTypeConvertFormat(buf *bytes.Buffer, a *model.Argument) {
	if a.IsArray != nil && a.IsArray.IsVariadic {
		buf.WriteString("[]")
		a = a.IsArray.Element
	}

	c.argumentTypeFormat(buf, a)
}

// ArgumentTypeConvertString converts method's argument type in string to pass it to the converting code.
// For example *pkg.Type -> v.(*pkg.Type) or ...*pkg.Type -> v.([]*pkg.Type).
func (c *Convertor) ArgumentTypeConvertString(a *model.Argument) string {
	buf := bytes.Buffer{}
	c.argumentTypeConvertFormat(&buf, a)
	return buf.String()
}

func (c *Convertor) argumentFormat(buf *bytes.Buffer, a *model.Argument) {
	if a.Name != "" {
		buf.WriteString(a.Name)
		buf.WriteByte(' ')
	}

	c.argumentTypeFormat(buf, a)
}

// ArgumentString converts method's argument in string.
func (c *Convertor) ArgumentString(a *model.Argument) string {
	buf := bytes.Buffer{}
	c.argumentFormat(&buf, a)
	return buf.String()
}

// MethodTuplesString returns tuples string of the method or func.
func (c *Convertor) MethodTuplesString(m *model.Method) string {
	buf := bytes.Buffer{}

	c.methodTuplesFormat(&buf, m)

	return buf.String()
}

// GetZeroValue returns zero value of the argument.
func (c *Convertor) GetZeroValue(arg *model.Argument) string {
	switch {

	// Basic value.
	case arg.BasicInfo != 0:
		switch {
		case arg.BasicInfo&types.IsNumeric != 0:
			return "0"
		case arg.BasicInfo&types.IsString != 0:
			return `""`
		case arg.BasicInfo&types.IsBoolean != 0:
			return "false"
		default:
			panic(fmt.Sprintf("unknown basic type: %d", arg.BasicInfo))
		}

	// Named value.
	case arg.IsNamed != nil:
		if arg.IsNamed.IsStruct != nil { // This is a named struct, just return pkg.Type{}.
			return fmt.Sprintf("%s{}", c.GetQualifiedType(arg.IsNamed.Package, arg.IsNamed.TypeName))
		}

		return c.GetZeroValue(arg.IsNamed)

	// Nilable values.
	case arg.IsPointer != nil, arg.IsArray != nil, arg.IsFunc != nil,
		arg.IsMap != nil, arg.IsChan != nil, arg.IsInterface != nil:
		return "nil"

	// Anonymous struct.
	case arg.IsStruct != nil:
		buf := bytes.Buffer{}
		c.structArgumentFormat(&buf, arg.IsStruct)
		return fmt.Sprintf("%s{}", buf.String())

	}

	panic(fmt.Sprintf("unknown zero value: %#v", arg))
}

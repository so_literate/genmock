package printer

import (
	"fmt"
	"strings"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

func setArgumentsName(prefix string, args []*model.Argument) {
	for i, arg := range args {
		if arg.Name != "" {
			continue
		}

		arg.Name = fmt.Sprintf("%s%d", prefix, i)
	}
}

func isPossibleReturnError(args []*model.Argument) bool {
	if len(args) == 0 {
		return false
	}

	lastRes := args[len(args)-1]

	if lastRes.IsNamed == nil {
		return false
	}

	lastRes = lastRes.IsNamed

	if lastRes.Package != "" || lastRes.TypeName != "error" {
		return false
	}

	return true
}

func getArgumentNames(args []*model.Argument) string {
	names := make([]string, len(args))

	for i, arg := range args {
		names[i] = arg.Name
	}

	return strings.Join(names, ", ")
}

func (p *Printer) getZeroValues(args []*model.Argument) string {
	if len(args) == 0 {
		return ""
	}

	values := make([]string, len(args))
	for i, arg := range args {
		values[i] = p.convertor.GetZeroValue(arg)
	}

	return strings.Join(values, ", ")
}

func isPossibleConvertNil(arg *model.Argument) bool {
	if arg.IsInterface != nil {
		return false
	}

	if arg.IsNamed != nil {
		return isPossibleConvertNil(arg.IsNamed)
	}

	return true
}

func getArgMethodName(argName string) string {
	argMethodName := argName
	if !strings.HasPrefix(argMethodName, "_") {
		argMethodName = fmt.Sprintf("_%s", argMethodName)
	}

	return argMethodName
}

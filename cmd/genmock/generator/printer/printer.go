// Package printer conatains code generator using analyzer's models.
package printer

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/iancoleman/strcase"
	"github.com/rs/zerolog"
	"golang.org/x/tools/imports"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

// ErrInvalidConfig returns when you create printer with invalid config params.
var ErrInvalidConfig = errors.New("invalid config")

type fileCase int

const (
	fileCaseSnake fileCase = iota + 1
	fileCaseCamel
)

const (
	pkgGenmock = "gitlab.com/so_literate/genmock"
)

// file generated file with path, name and genereted code.
type file struct {
	Path    string
	Content []byte
}

type nopCloser struct {
	io.Writer
}

func (nopCloser) Close() error { return nil }

// Printer creates new genereted files with interface implementers.
type Printer struct {
	log     zerolog.Logger
	conf    *config.Print
	version string

	fileCase  fileCase
	files     []*file
	convertor *Convertor

	stdout io.Writer
}

// New validate config and created new code printer.
func New(log *zerolog.Logger, conf *config.Print, stdout io.Writer, version string) (*Printer, error) {
	p := &Printer{
		log:     *log,
		conf:    conf,
		version: version,
		stdout:  stdout,
	}

	switch conf.File.Case {
	case "snake":
		p.fileCase = fileCaseSnake
	case "camel":
		p.fileCase = fileCaseCamel
	default:
		return nil, fmt.Errorf("%w: file case is invalid %q", ErrInvalidConfig, conf.File.Case)
	}

	return p, nil
}

func (p *Printer) getOutWriter(path string) (io.WriteCloser, error) {
	if p.conf.Place.Stdout {
		return nopCloser{p.stdout}, nil
	}

	pathDir := filepath.Dir(path)

	_, err := os.Stat(pathDir)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return nil, fmt.Errorf("os.Stat: %w", err)
		}

		if err = os.MkdirAll(pathDir, 0o755); err != nil { //nolint:gosec,gomnd // It's make dir with 0755.
			return nil, fmt.Errorf("os.MkdirAll (%s): %w", pathDir, err)
		}
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0o644) //nolint:gosec,gomnd // It's ok 'Potential file inclusion via variable'.
	if err != nil {
		return nil, fmt.Errorf("os.OpenFile: %w", err)
	}

	if err = f.Truncate(0); err != nil {
		return nil, fmt.Errorf("f.Truncate: %w", err)
	}

	if _, err = f.Seek(0, 0); err != nil {
		return nil, fmt.Errorf("f.Seek: %w", err)
	}

	return f, nil
}

func (p *Printer) outFiles() error {
	for _, f := range p.files {
		out, err := p.getOutWriter(f.Path)
		if err != nil {
			return fmt.Errorf("p.getOutWriter: %w", err)
		}

		_, err = fmt.Fprintf(out, "%s", f.Content)
		if err != nil {
			return fmt.Errorf("fmt.Fprintf: %w", err)
		}

		if err = out.Close(); err != nil {
			return fmt.Errorf("out.Close: %w", err)
		}

		p.log.Info().Msgf("Generated mock into: %s", f.Path)
	}

	return nil
}

// Run runs new code generator.
// Generates code of the interface implementers and saves it to files.
func (p *Printer) Run(ctx context.Context, ifaces []*model.Interface) error {
	p.log.Debug().
		Int("ifaces", len(ifaces)).
		Interface("conf", *p.conf).
		Msg("running printer")

	p.files = make([]*file, len(ifaces))
	p.convertor = NewConvertor()

	// register required imports.
	p.convertor.GetPkgPathName(pkgGenmock)
	p.convertor.GetPkgPathName("fmt")

	var err error
	for i, iface := range ifaces {
		p.files[i], err = p.generateFile(iface)
		if err != nil {
			return fmt.Errorf("generateFile %s.%s: %w", iface.ImportPath, iface.Name, err)
		}
	}

	if err = p.outFiles(); err != nil {
		return fmt.Errorf("outFiles: %w", err)
	}

	return nil
}

// GetFileName returns file name of based on interface name and config.
func (p *Printer) GetFileName(ifaceName string) string {
	switch p.fileCase {
	case fileCaseSnake:
		ifaceName = strcase.ToSnake(ifaceName)
	case fileCaseCamel:
		// ifaceName already in camel case (Go naming).
	}

	tmpl := "%s.go"
	if p.conf.File.Test {
		tmpl = "%s_test.go"
	}

	return fmt.Sprintf(tmpl, ifaceName)
}

// GetFilePath returns full path to the new file based on config and interface location.
func (p *Printer) GetFilePath(iface *model.Interface) string {
	fileName := p.GetFileName(iface.Name)

	if p.conf.Place.Stdout {
		return ""
	}

	path := p.conf.Place.Path

	if p.conf.Place.InPackage {
		path = iface.DirPath
	}

	if p.conf.Place.KeepTree {
		path = filepath.Join(path, iface.ImportPath)
	}

	return filepath.Join(path, fileName)
}

// getFileImport returns import path of the new file.
// Returns interface import path if you want to generate file:
// - in the same package
// - without keep tree struct
// - without _test package suffix
// In other cases returns empty import path, because you have to import everything in external package.
func (p *Printer) getFileImport(iface *model.Interface) string {
	if p.conf.Place.InPackage && !p.conf.Place.KeepTree && !p.conf.Package.Test {
		return iface.ImportPath
	}

	return ""
}

type writer struct {
	buf bytes.Buffer
}

func (w *writer) printf(format string, args ...interface{}) {
	fmt.Fprintf(&w.buf, format, args...)
	w.buf.WriteByte('\n')
}

func (p *Printer) generateFile(iface *model.Interface) (*file, error) {
	res := &file{
		Path: p.GetFilePath(iface),
	}

	p.convertor.SetFileParams(p.getFileImport(iface))

	content := &writer{buf: bytes.Buffer{}}

	// Prints header and package of the file.
	p.printHeadAndPackage(content, iface)

	// Prints full content of the file into external writer.
	body := &writer{buf: bytes.Buffer{}}
	p.printImplementer(body, iface.Name)

	for _, m := range iface.Methods {
		p.printImplementerMethod(body, iface.Name, m)
	}

	for _, m := range iface.Methods {
		p.printSetterMethod(body, iface.Name, m)
	}

	// Prints import block of the file.
	p.printImport(content)

	// Prints content to file.
	body.buf.WriteTo(&content.buf) //nolint:errcheck,gosec // don't care about this error.

	var err error
	res.Content, err = imports.Process("", content.buf.Bytes(), nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", content.buf.Bytes())
		return nil, fmt.Errorf("format source: %w", err)
	}

	return res, nil
}

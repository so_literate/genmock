package printer

import (
	"fmt"
	"strings"

	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
)

const implReceiverName = "_m"

func getImplementerTypeName(ifaceName string) string {
	return fmt.Sprintf("%s_Mock", ifaceName)
}

func (p *Printer) printImplementer(w *writer, ifaceName string) {
	impl := getImplementerTypeName(ifaceName)

	testingT := p.convertor.GetQualifiedType(pkgGenmock, "TestingT")

	w.printf("// %s implements mock of the %s interface.", impl, ifaceName)
	w.printf("type %s struct {", impl)
	w.printf("	Mock *%s", p.convertor.GetQualifiedType(pkgGenmock, "Mock"))
	w.printf("	t    %s", testingT)
	w.printf("")
	w.printf("	ReturnMockErrorAsResult bool")
	w.printf("}")
	w.printf("")
	w.printf("// New%s returns a new mock of %s interface implementer.", impl, ifaceName)
	w.printf("// Takes *testing.T to stop failed testing process.")
	w.printf("func New%s(t %s) *%s {", impl, testingT, impl)
	w.printf("	return &%s{", impl)
	w.printf("		Mock: %s(),", p.convertor.GetQualifiedType(pkgGenmock, "NewMock"))
	w.printf("		t:    t,")
	w.printf("	}")
	w.printf("}")
	w.printf("")
}

func (p *Printer) printReturnMockErrorAsBusiness(w *writer, args []*model.Argument, errTmpl string) {
	fatalToCallMethod := fmt.Sprintf(`%s.t.Fatalf("%s: %%s", _err)`, implReceiverName, errTmpl)

	if !isPossibleReturnError(args) {
		w.printf(`	%s`, fatalToCallMethod)
		return
	}

	mockError := fmt.Sprintf(`%s("%s: %%w", _err)`, p.convertor.GetQualifiedType("fmt", "Errorf"), errTmpl)

	zeroValues := p.getZeroValues(args[:len(args)-1])
	if zeroValues != "" {
		mockError = fmt.Sprintf("%s, %s", zeroValues, mockError)
	}

	w.printf(`if %s.ReturnMockErrorAsResult {`, implReceiverName)
	w.printf(`	return %s`, mockError)
	w.printf(`}`)
	w.printf(`%s`, fatalToCallMethod)
}

func (p *Printer) printConvertReturnedResult(w *writer, index int, method *model.Method) {
	result := method.Results[index]

	if strings.HasPrefix(result.Name, "_") { // prints it only own name of results.
		w.printf(`var %s %s`, result.Name, p.convertor.ArgumentTypeString(result))
	}

	retByIndex := fmt.Sprintf("_ret[%d]", index)
	if !isPossibleConvertNil(result) {
		// You can't to convert nil interface to target type.
		w.printf(`if _r := %s; _r != nil {`, retByIndex)

		retByIndex = "_r"
	}

	w.printf(`if _v, _ok := %s.(%s); _ok {`, retByIndex, p.convertor.ArgumentTypeString(result))
	w.printf(`	%s = _v`, result.Name)
	w.printf(`} else {`)
	w.printf(`	_err = %s("%%w [ret #%d]: want '%s', got: %%[2]T(%%#[2]v)", %s, %s)`,
		p.convertor.GetQualifiedType("fmt", "Errorf"),
		index,
		p.convertor.ArgumentTypeString(result),
		p.convertor.GetQualifiedType(pkgGenmock, "ErrUnexpectedArgumentType"),
		retByIndex,
	)
	p.printReturnMockErrorAsBusiness(w, method.Results, fmt.Sprintf("(%s) check returned type", method.Name))
	w.printf(`}`)

	if !isPossibleConvertNil(result) { // Have to close bracket of top if.
		w.printf(`}`)
	}

	w.printf("")
}

// printImplementerMethod prints method to implement interface.
func (p *Printer) printImplementerMethod(w *writer, ifaceName string, method *model.Method) {
	setArgumentsName("_a", method.Params)

	w.printf(fmt.Sprintf(`func (%s *%s) %s%s {`,
		implReceiverName, getImplementerTypeName(ifaceName), method.Name, p.convertor.MethodTuplesString(method)),
	)

	setArgumentsName("_r", method.Results)

	calledArgs := getArgumentNames(method.Params)
	methodCalledArgs := fmt.Sprintf("%q", method.Name)

	if calledArgs != "" {
		methodCalledArgs = fmt.Sprintf("%s, %s", methodCalledArgs, calledArgs)
	}

	// Mark method as test helper.
	w.printf(`%s.t.Helper()`, implReceiverName)
	w.printf("")

	// Try to find registered caller.
	w.printf(`_ret, _err := %s.Mock.MethodCalled(%s)`, implReceiverName, methodCalledArgs)
	w.printf(`if _err != nil {`)
	p.printReturnMockErrorAsBusiness(w, method.Results, fmt.Sprintf("(%s) call mock method", method.Name))
	w.printf(`}`)
	w.printf("")

	// Check length of the returned params.
	w.printf(`if len(_ret) != %d {`, len(method.Results))
	w.printf(`	_err = %s("%%w: want %d, got %%d", %s, len(_ret))`,
		p.convertor.GetQualifiedType("fmt", "Errorf"),
		len(method.Results),
		p.convertor.GetQualifiedType(pkgGenmock, "ErrWrongReturnedLenght"),
	)
	p.printReturnMockErrorAsBusiness(w, method.Results, fmt.Sprintf("(%s) check length of returned params", method.Name))
	w.printf(`}`)

	if len(method.Results) == 0 {
		w.printf(`}`)
		w.printf("")
		return
	}

	w.printf("")

	// Convert returned params.
	ret := make([]string, len(method.Results))
	for i := range method.Results {
		p.printConvertReturnedResult(w, i, method)
		ret[i] = method.Results[i].Name
	}

	w.printf("return %s", strings.Join(ret, ", "))

	w.printf(`}`)
	w.printf("")
}

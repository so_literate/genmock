package printer

func (p *Printer) printImport(w *writer) {
	w.printf("import (")
	p.convertor.printFileImports(&w.buf)
	w.printf(")")
	w.printf("")
}

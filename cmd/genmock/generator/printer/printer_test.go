package printer_test

import (
	"bytes"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/model"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/printer"
)

func preparePrinter(so *require.Assertions, conf *config.Print) *printer.Printer {
	buf := new(bytes.Buffer)
	log := zerolog.New(buf)

	stdOut := new(bytes.Buffer)

	p, err := printer.New(&log, conf, stdOut, "")
	so.NoError(err)

	return p
}

func TestGetFileName(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	testCase := func(p *printer.Printer, exp string) {
		actual := p.GetFileName("InterfaceName")
		so.Equal(exp, actual)
	}

	conf := &config.Print{
		File: config.File{
			Case: "snake",
			Test: false,
		},
	}

	p := preparePrinter(so, conf)
	testCase(p, "interface_name.go")
	conf.File.Test = true
	testCase(p, "interface_name_test.go")

	conf.File.Case = "camel"
	p = preparePrinter(so, conf)
	testCase(p, "InterfaceName_test.go")
	conf.File.Test = false
	testCase(p, "InterfaceName.go")
}

func TestGetFilePath(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Print{
		File: config.File{
			Case: "snake",
			Test: true,
		},
		Place: config.Place{
			Path: "./mock",
		},
	}

	p := preparePrinter(so, conf)

	testCase := func(exp string) {
		actual := p.GetFilePath(&model.Interface{
			Name:        "Iface",
			PackageName: "pkg",
			ImportPath:  "host.com/rEpO/project-name/pkg",
			DirPath:     "/home/username/go/src/host.com/rEpO/project-name/pkg",
		})
		so.Equal(exp, actual)
	}

	// Default config
	testCase("mock/iface_test.go")

	// Have to create new file in package with interface.
	conf.Place.InPackage = true
	testCase("/home/username/go/src/host.com/rEpO/project-name/pkg/iface_test.go")

	// Have to create new file in package with interface and import path.
	conf.Place.KeepTree = true
	testCase("/home/username/go/src/host.com/rEpO/project-name/pkg/host.com/rEpO/project-name/pkg/iface_test.go")

	// have to keep import path without import path.
	conf.Place.InPackage = false
	testCase("mock/host.com/rEpO/project-name/pkg/iface_test.go")

	// Print file to stdout.
	conf.Place.Stdout = true
	testCase("")
}

// Package generator contains code generator for genmock core.
// It goes through the user code and generates objects and methods over interfaces.
package generator

import (
	"context"
	"fmt"
	"io"

	"github.com/rs/zerolog"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/analyzer"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator/printer"
	"gitlab.com/so_literate/genmock/cmd/genmock/logger"
)

const version = "v0.0.8"

// Generator main type runs analyzer and code printer.
type Generator struct {
	conf *config.Config
	log  *zerolog.Logger

	analyzer *analyzer.Analyzer
	printer  *printer.Printer
}

// New returns generator with logger ready to run.
func New(conf *config.Config, stdout io.Writer) (g *Generator, err error) {
	g = &Generator{
		conf: conf,
		log:  logger.New(&conf.Log),
	}

	g.analyzer = analyzer.New(g.log, &g.conf.Search)

	g.printer, err = printer.New(g.log, &g.conf.Print, stdout, version)
	if err != nil {
		return nil, fmt.Errorf("printer.New: %w", err)
	}

	return g, nil
}

// Run runs analyzer and code printer.
func (g *Generator) Run(ctx context.Context) error {
	if g.conf.Version {
		g.log.Info().Msg(version)
		return nil
	}

	g.log.Info().Msgf("Starting genmock: %s", version)

	if g.conf.Timeout != 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, g.conf.Timeout)
		defer cancel()
	}

	ifaces, err := g.analyzer.Run(ctx)
	if err != nil {
		return fmt.Errorf("analyzer.Run: %w", err)
	}

	if len(ifaces) != 0 {
		if err = g.printer.Run(ctx, ifaces); err != nil {
			return fmt.Errorf("printer.Run: %w", err)
		}
	}

	g.log.Info().Msgf("Generated %d interface(s)", len(ifaces))

	return nil
}

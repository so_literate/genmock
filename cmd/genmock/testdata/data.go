package testdata

import (
	"context"
	"errors"
	"io"

	"gitlab.com/so_literate/genmock/cmd/genmock/testdata/model"
	skip "gitlab.com/so_literate/genmock/cmd/genmock/testdata/skipdir"
)

var ErrVarIsInterface = errors.New("this is the interface but...")

type Iface interface {
	Method(i int64) error
}

type EmbeddedInterface interface {
	MethodOfEmbeddedInterface(int)
}

type Clean struct {
	Value int64
}

type (
	NamedPointer *model.Data
	NamedInt     int
	NamedChan    chan<- *model.Data
	NamedSlice   []Clean
	NamedArray   [10]Clean
	NamedMap     map[Clean]map[int][]*model.Data
	NamedFunc    func(id int, data *model.Data, variadic ...string) (i int64, err error)
)

type AllVariants interface {
	EmbeddedInterface
	io.Reader
	skip.Skipped

	AnonEmbeddedStruct(s struct{ Clean })
	AnonStruct(s struct{ data int })
	Basic(id int64)
	BasicPointer(id *int64)
	ChanMethod(ch chan *model.Data)
	ChanMethodDir1(ch chan<- *model.Data)
	ChanMethodDir2(ch <-chan *model.Data)
	ChanMethodWithChan(ch chan (<-chan *model.Data))
	ChanNamedMethod(ch NamedChan)
	Clean(Clean)
	CleanAll([]Clean)
	CleanAny(...Clean)
	CleanAnyAll(...[]Clean)
	CleanFull([10]Clean)
	CleanNamed(NamedSlice)
	CleanNamedFull(NamedArray)
	ContextMethod(ctx context.Context)
	Func(fn func(id int, data *model.Data, variadic ...string) (i int64, err error)) error
	FuncNamed(fn NamedFunc)
	HiddenType(a1, a2 int64)
	InterfaceAnon(i interface{ Method(i int) error })
	InterfaceEmpty(i interface{})
	InterfaceSkipped(skip.Skipped)
	InterfaceStd(io.Closer)
	Maps(map[int]*model.Data) map[Clean]map[int][]*model.Data
	MapsNamed(NamedMap) NamedMap
	NamedArgs(id int64) (m *model.Data, err error)
	NamedIntArg(id NamedInt)
	Pointer(*model.Data) error
	PointerNamed(NamedPointer)
	Unsafe(u uintptr)
}

// Package config contains configuration of the generator.
package config

import (
	"fmt"
	"time"

	"gitlab.com/so_literate/fconfig"
)

// Log configuration.
type Log struct {
	Debug   bool `default:"false" usage:"prints debug messages of the generator"`
	Quiet   bool `default:"false" usage:"does not print any messages other than errors"`
	Pretty  bool `default:"true" usage:"prints pretty log messages"`
	NoColor bool `default:"false" usage:"prints log messages without color"`
}

// Search is a config of the interface searching.
type Search struct {
	Name            string   `default:".*" usage:"name or golang regexp of the interface ('.*' means all names)"`
	Root            string   `default:"." usage:"root directory of the searching"`
	Recursive       bool     `default:"false" usage:"enables to scan subdirectories"`
	SkipPaths       []string `usage:"skips some paths to find interfaces, example: './vendor,./exclude_dir'"`
	IncludeTests    bool     `default:"false" usage:"includes test packages to scan"`
	IncludeNotTypes bool     `default:"false" usage:"includes 'var' and other statements"`
}

// File is a naming rules of the new files.
type File struct {
	Case string `default:"snake" enum:"snake,camel" usage:"rules of the generated file name 'iface_name.go'"`
	Test bool   `default:"false" usage:"generated file name will be with the suffix '_test.go'"`
}

// Place of the new file.
type Place struct {
	Path      string `default:"./mock" usage:"path to write generated files"`
	KeepTree  bool   `default:"false" usage:"keep the tree structure of the interface files"`
	InPackage bool   `default:"false" usage:"generates files inside of the package with interface"`
	Stdout    bool   `default:"false" usage:"prints generated files to stdout"`
}

// Package rules of the package generate.
type Package struct {
	Name string `default:"mock" usage:"name of the package in generated package"`
	Test bool   `default:"false" usage:"package with '_test' suffix"`
}

// Content additional content of the generated code.
type Content struct {
	HideVersion bool   `default:"false" usage:"do not prints app version in the generated files"`
	Tags        string `default:"" usage:"prints additional tags to generated files, example: '+build some_tag'"`
}

// Print is a config of the rules and destination of the generated files.
type Print struct {
	File    File
	Place   Place
	Package Package
	Content Content
}

// Config full configuration of the app.
type Config struct {
	Log    Log
	Search Search
	Print  Print

	Timeout time.Duration `default:"1m" usage:"timeout for analysis and code printer"`
	Version bool          `default:"false" usage:"prints version of the app"`
}

// Load parse config params and returns app config.
func Load() (*Config, error) {
	config := new(Config)

	loader := fconfig.New(config).PrefixEnv("GENMOCK").FileFlagConfig("config").FileNotFoundAlert()

	if err := loader.Load(); err != nil {
		return nil, fmt.Errorf("fconfig.Load: %w", err)
	}

	return config, nil
}

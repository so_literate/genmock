// Package main contains the code to run the application.
package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/so_literate/graceful"

	"gitlab.com/so_literate/genmock/cmd/genmock/config"
	"gitlab.com/so_literate/genmock/cmd/genmock/generator"
)

// Run creates and runs mock generator.
func Run(ctx context.Context, conf *config.Config, stdout io.Writer) error {
	gen, err := generator.New(conf, stdout)
	if err != nil {
		return fmt.Errorf("generator.New: %w", err)
	}

	if err = gen.Run(ctx); err != nil {
		return fmt.Errorf("generator.Run: %w", err)
	}

	return nil
}

func main() {
	log.SetFlags(0)

	run := func(ctx context.Context) error {
		conf, err := config.Load()
		if err != nil {
			return fmt.Errorf("config.Load: %w", err)
		}

		return Run(ctx, conf, os.Stdout)
	}

	graceful.TimeoutShutdown = time.Second

	if err := graceful.RunFuncs(run); err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}

package main_test

import (
	"bytes"
	"context"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	main "gitlab.com/so_literate/genmock/cmd/genmock"
	"gitlab.com/so_literate/genmock/cmd/genmock/config"
)

func TestRun(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Config{
		Log: config.Log{
			Quiet: true,
		},
		Search: config.Search{
			Name: "AllVariants",
			Root: filepath.Join(".", "testdata"),
		},
		Print: config.Print{
			File: config.File{
				Case: "snake",
				Test: true,
			},
			Place: config.Place{
				InPackage: true,
				Stdout:    true,
			},
			Package: config.Package{
				Name: "testdata",
			},
		},
		Timeout: time.Second * 10,
	}

	stdout := new(bytes.Buffer)

	err := main.Run(context.Background(), conf, stdout)
	so.NoError(err)

	savedFile, err := os.ReadFile(filepath.Join(".", "testdata", "all_variants.go.txt"))
	so.NoError(err)

	so.Equal(string(savedFile), stdout.String())
}

func TestRunToFile(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	path := filepath.Join(t.TempDir(), "mock")

	conf := &config.Config{
		Log: config.Log{
			Quiet: true,
		},
		Search: config.Search{
			Name: "Iface",
			Root: filepath.Join(".", "testdata"),
		},
		Print: config.Print{
			File: config.File{
				Case: "snake",
				Test: true,
			},
			Place: config.Place{
				Path:     path,
				KeepTree: true,
			},
			Package: config.Package{
				Name: "mock",
				Test: true,
			},
		},
		Timeout: time.Second * 10,
	}

	err := main.Run(context.Background(), conf, nil)
	so.NoError(err)

	savedFile, err := os.ReadFile(filepath.Join(path, "gitlab.com", "so_literate", "genmock", "cmd", "genmock", "testdata", "iface_test.go"))
	so.NoError(err)
	so.Greater(len(savedFile), 1000) // just not empty file
}

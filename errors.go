package genmock

import (
	"errors"
)

var (
	// ErrMethodUnknown returns when method do not have registered calls.
	ErrMethodUnknown = errors.New("method unknown")
	// ErrLengthNotEqual returns in comparison of the arguments and the length of their different.
	ErrLengthNotEqual = errors.New("length is not equal")
	// ErrIsEqualNotTrue returns when custom matcher IsEqual returned false.
	ErrIsEqualNotTrue = errors.New("matcher IsEqual is not true")
	// ErrDeepEqualNotTrue returns when reflect.DeepEqual comparison retruned false.
	ErrDeepEqualNotTrue = errors.New("deep equal is not true")
	// ErrArgumentTypeUnknown returns when you used AnythingOfType as an argument
	// and reflect.TypeOf can't to got type of the argument.
	ErrArgumentTypeUnknown = errors.New("type of the argument unknown")
	// ErrUnexpectedArgumentType returns when you used AnythingOfType as an argument
	// and type of the actual argument is different.
	ErrUnexpectedArgumentType = errors.New("type of the argument is unexpected")
	// ErrExpectedCallNotFound returns when expected call of the method not found.
	// Maybe you used Anything or AnythingOfType or you just got wrong values of the actual arguments.
	ErrExpectedCallNotFound = errors.New("expected call not found")
	// ErrSimilarCallNotFound returns when mock can't to found even similar call of the method.
	// Mock checked all calls with Anything/AnythingOfType arguments but nothing.
	ErrSimilarCallNotFound = errors.New("similar call not found")
	// ErrRemainsSeveralTimesCallMethods returns when you mock has
	// mathods to call but nobody call them.
	ErrRemainsSeveralTimesCallMethods = errors.New("it remains several times to call method(s)")
	// ErrIndexOutOfRange returns in converting type methods to avoid panic.
	ErrIndexOutOfRange = errors.New("index out of range")
	// ErrWrongReturnedLenght returns when list of the result arguments have an unexpected length.
	ErrWrongReturnedLenght = errors.New("length of the returned params is wrong")
)

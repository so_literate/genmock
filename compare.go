package genmock

import (
	"fmt"
	"reflect"
)

// IsArgumentsEquals checks arguments they are should be equal
// or expected argument should be matcher function.
func IsArgumentsEquals(expArg, actualArg interface{}) error {
	if fn, ok := expArg.(func(interface{}) bool); ok {
		if ok = fn(actualArg); !ok {
			return ErrIsEqualNotTrue
		}
		return nil
	}

	if !reflect.DeepEqual(expArg, actualArg) {
		return fmt.Errorf("%w: expected %[2]T(%#[2]v), actual %[3]T(%#[3]v)",
			ErrDeepEqualNotTrue, expArg, actualArg)
	}

	return nil
}

// IsArgumentListsEquals compares two lists of the arguments.
// Arguments should be equal by type and value.
func IsArgumentListsEquals(expArgs, actualArgs []interface{}) error {
	if len(expArgs) != len(actualArgs) {
		return fmt.Errorf("%w: want %d, got %d", ErrLengthNotEqual, len(expArgs), len(actualArgs))
	}

	for i, expArg := range expArgs {
		if err := IsArgumentsEquals(expArg, actualArgs[i]); err != nil {
			return fmt.Errorf("arguments are not equals: %w", err)
		}
	}

	return nil
}

// IsArgumentsSimilar compares arguments they are may be Anything, AnythingOfType
// or equal by type and value.
func IsArgumentsSimilar(expArg, actualArg interface{}) error {
	switch tp := expArg.(type) {
	case AnythingArg: // anything is good.
		return nil

	case AnythingOfTypeArg: // we want just the same type here.
		actualArgType := reflect.TypeOf(actualArg)
		if actualArgType == nil {
			return fmt.Errorf("%w: %#v", ErrArgumentTypeUnknown, actualArg)
		}

		actualTypeName := actualArgType.String()

		if actualTypeName != string(tp) {
			return fmt.Errorf("%w: want: %q, got: %q(%T)", ErrUnexpectedArgumentType, tp, actualTypeName, actualArg)
		}

		return nil
	}

	return IsArgumentsEquals(expArg, actualArg)
}

// IsArgumentListsSimilar compare two lists of the arguments.
// Arguments may contains less accurate comparison types (Anything, AnythingOfType).
func IsArgumentListsSimilar(expArgs, actualArgs []interface{}) error {
	if len(expArgs) != len(actualArgs) {
		return fmt.Errorf("%w: want %d, got %d", ErrLengthNotEqual, len(expArgs), len(actualArgs))
	}

	for i, expArg := range expArgs {
		if err := IsArgumentsSimilar(expArg, actualArgs[i]); err != nil {
			return fmt.Errorf("arguments are not similar: %w", err)
		}
	}

	return nil
}

package genmock_test

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/so_literate/genmock"
)

var (
	errTest  = errors.New("test error")
	errTest2 = errors.New("test error 2")
)

func TestNewCall(t *testing.T) {
	t.Parallel()

	// Call like a db.SetData(i int) error.
	call := genmock.NewCall("SetData", []interface{}{10}, []interface{}{errTest}, 1)
	if call == nil {
		t.Fatal("call is nil")
	}

	if call.MethodName != "SetData" {
		t.Fatalf("field MethodName is wrong: %q", call.MethodName)
	}

	if call.Times != 1 {
		t.Fatalf("field Times is wrong: %d", call.Times)
	}

	if len(call.Args) != 1 {
		t.Fatalf("length of the args is wrong: %d", len(call.Args))
	}

	i, ok := call.Args[0].(int)
	if !ok {
		t.Fatalf("type of the first arg is wrong: %#v", call.Args[0])
	}

	if i != 10 {
		t.Fatalf("value of the first arg is wrong: %#v", call.Args[0])
	}

	err, ok := call.Returns[0].(error)
	if !ok {
		t.Fatalf("type of the first return arg is wrong: %#v", call.Returns[0])
	}

	if !errors.Is(err, errTest) {
		t.Fatalf("value of the first return arg is wrong: %#v", call.Returns[0])
	}
}

func TestMockPositive(t *testing.T) {
	t.Parallel()

	// Call like a db.SetData(10 int) error.
	callSetData := genmock.NewCall("SetData", []interface{}{10}, []interface{}{errTest}, 1)

	// Call like a db.GetData(i int) (int, error).
	callGetData := genmock.NewCall(
		"GetData",
		[]interface{}{genmock.AnythingOfType("int")},
		[]interface{}{10, nil},
		genmock.TimesAny,
	)

	// Call like a db.SetData(i ???) error.
	callSetDataAny := genmock.NewCall("SetData", []interface{}{genmock.Anything}, []interface{}{errTest2}, 1)

	mock := genmock.NewMock()
	mock.AddCall(callSetData).
		AddCall(callGetData).
		AddCall(callSetDataAny)

	// Expected SetData method.
	retArgs, err := mock.MethodCalled("SetData", 10)
	if err != nil {
		t.Fatalf("error in call correct method: %v", err)
	}

	err = retArgs[0].(error)
	if !errors.Is(err, errTest) {
		t.Fatalf("value of the returning arg is wrong: %#v", retArgs[0])
	}

	// Similar GetData method.
	retArgs, err = mock.MethodCalled("GetData", 1)
	if err != nil {
		t.Fatalf("error in call correct method: %v", err)
	}

	if i := retArgs[0].(int); i != 10 {
		t.Fatalf("wrong first returning arg: %v", i)
	}

	if retArgs[1] != nil {
		t.Fatalf("wrong second returning arg: %#v", retArgs[1])
	}

	// Similar SetData method.
	retArgs, err = mock.MethodCalled("SetData", "string is anything")
	if err != nil {
		t.Fatalf("error in call correct method: %v", err)
	}

	err = retArgs[0].(error)
	if !errors.Is(err, errTest2) {
		t.Fatalf("value of the returning arg is wrong: %#v", retArgs[0])
	}

	// Checks that all methods were called as expected.
	err = mock.AssertExpectations()
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestMockNegative(t *testing.T) {
	t.Parallel()

	mock := genmock.NewMock()

	_, err := mock.MethodCalled("unexpected_method")
	if !errors.Is(err, genmock.ErrMethodUnknown) {
		t.Fatalf("wrong error of unexpected method: %v", err)
	}

	mock.AddCall(genmock.NewCall("method", []interface{}{1}, []interface{}{nil}, 1))

	_, err = mock.MethodCalled("method", 10)
	if !errors.Is(err, genmock.ErrSimilarCallNotFound) {
		t.Fatalf("wrong error of expected method: %v", err)
	}

	t.Logf("ErrSimilarCallNotFound text:\n%s", err)

	_, err = mock.MethodCalled("method", 1)
	if err != nil {
		t.Fatalf("wrong error of expected method: %v", err)
	}

	_, err = mock.MethodCalled("method", 1)
	if !errors.Is(err, genmock.ErrSimilarCallNotFound) {
		t.Fatalf("wrong error of expected method: %v", err)
	}

	mock.AddCall(genmock.NewCall("method", []interface{}{}, []interface{}{}, 1))

	err = mock.AssertExpectations()
	if !errors.Is(err, genmock.ErrRemainsSeveralTimesCallMethods) {
		t.Fatalf("wrong error of AssertExpectations: %v", err)
	}

	t.Logf("ErrRemainsSeveralTimesCallMethods text\n%s", err)
}

func TestNoPanicOnEmptyMock(t *testing.T) {
	t.Parallel()

	mock := new(genmock.Mock)
	mock.AddCall(genmock.NewCall("call", nil, nil, 1))

	ret, err := mock.MethodCalled("call")
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	if ret != nil {
		t.Fatalf("unexpected returned arguments: %#v", ret)
	}

	if err = mock.AssertExpectations(); err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
}

func TestWaitFor(t *testing.T) {
	t.Parallel()

	call := genmock.NewCall("method", nil, nil, -1)
	ch := make(chan time.Time)

	call.WaitFor = ch

	sentSignal := false
	go func() {
		time.Sleep(time.Millisecond * 10)
		sentSignal = true
		ch <- time.Now()
	}()

	mock := genmock.NewMock().AddCall(call)

	_, err := mock.MethodCalled("method")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if !sentSignal {
		t.Fatalf("no signal was sent")
	}
}

func TestWaitTime(t *testing.T) {
	t.Parallel()

	call := genmock.NewCall("method", nil, nil, -1)

	call.WaitTime = time.Millisecond * 50

	mock := genmock.NewMock().AddCall(call)

	now := time.Now()

	_, err := mock.MethodCalled("method")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	dur := time.Since(now)

	if int64(dur) < int64(time.Millisecond*50) {
		t.Fatalf("the method did not wait enough time: %d", dur)
	}
}

func TestRunFn(t *testing.T) {
	t.Parallel()

	call := genmock.NewCall("method", []interface{}{1}, nil, -1)

	funcCalled := false

	call.RunFn = func(args []interface{}) {
		if len(args) != 1 {
			t.Fatalf("wrong args list: %#v", args)
		}

		value, ok := args[0].(int)
		if !ok {
			t.Fatalf("wrong argument type: %#v", args[0])
		}

		if value != 1 {
			t.Fatalf("wrong argument value: %d", value)
		}

		funcCalled = true
	}

	mock := genmock.NewMock().AddCall(call)

	_, err := mock.MethodCalled("method", 1)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if !funcCalled {
		t.Fatalf("function did not run")
	}
}

func TestPanicMsg(t *testing.T) {
	t.Parallel()

	call := genmock.NewCall("method", nil, nil, -1)

	pmsg := "panic message"
	call.PanicMsg = &pmsg

	mock := genmock.NewMock().AddCall(call)

	actualPanicText := ""

	func() {
		defer func() {
			if r := recover(); r != nil {
				var ok bool
				actualPanicText, ok = r.(string)
				if !ok {
					t.Fatalf("another panic in the func: %#v", r)
				}
			}
		}()

		mock.MethodCalled("method")
	}()

	if pmsg != actualPanicText {
		t.Fatalf("actual panic text is wrong: want %q, got %q", pmsg, actualPanicText)
	}
}

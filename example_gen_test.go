package genmock_test

import (
	"fmt"
	"log"

	"gitlab.com/so_literate/genmock"
)

// Example of using the generated code with mock interface.
func Example() {
	iface := NewIface_Mock(genmock.NewLoggerHelper(log.Default())) // Pass *testing.T here.
	iface.ReturnMockErrorAsResult = true                           // You can enable returning mock error as result of the method.

	// Create and set argument and return parameters of the method.
	iface.On_Method().Args(1).Rets(nil).Times(genmock.TimesAny)

	err := iface.Method(1) // Call the method.
	fmt.Println(err)

	err = iface.Mock.AssertExpectations() // Check that all method called.
	fmt.Println(err)

	// Output:
	// <nil>
	// <nil>
}

// Iface_Mock implements mock of the Iface interface.
type Iface_Mock struct {
	Mock *genmock.Mock
	t    genmock.TestingT

	ReturnMockErrorAsResult bool
}

// NewIface_Mock returns a new mock of Iface interface implementer.
// Takes *testing.T to stop failed testing process.
func NewIface_Mock(t genmock.TestingT) *Iface_Mock {
	return &Iface_Mock{
		Mock: genmock.NewMock(),
		t:    t,
	}
}

func (_m *Iface_Mock) Method(i int64) error {
	_ret, _err := _m.Mock.MethodCalled("Method", i)
	if _err != nil {
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(Method) call mock method: %w", _err)
		}
		_m.t.Fatalf("(Method) call mock method: %s", _err)
	}

	if len(_ret) != 1 {
		_err = fmt.Errorf("%w: want 1, got %d", genmock.ErrWrongReturnedLenght, len(_ret))
		if _m.ReturnMockErrorAsResult {
			return fmt.Errorf("(Method) check length of returned params: %w", _err)
		}
		_m.t.Fatalf("(Method) check length of returned params: %s", _err)
	}

	var _r0 error
	if _r := _ret[0]; _r != nil {
		if _v, _ok := _r.(error); _ok {
			_r0 = _v
		} else {
			_err = fmt.Errorf("%w [ret #0]: want 'error', got: %[2]T(%#[2]v)", genmock.ErrUnexpectedArgumentType, _r)
			if _m.ReturnMockErrorAsResult {
				return fmt.Errorf("(Method) check returned type: %w", _err)
			}
			_m.t.Fatalf("(Method) check returned type: %s", _err)
		}
	}

	return _r0
}

// Iface_MockSet_Method allows to set arguments and results of the mock call Method.
type Iface_MockSet_Method struct {
	Call *genmock.Call
}

// On_Method adds a call of the Method to mock.
func (_m *Iface_Mock) On_Method() Iface_MockSet_Method {
	call := genmock.NewCall(
		"Method",
		[]interface{}{genmock.AnythingOfType("int64")},
		[]interface{}{nil},
		1,
	)
	_m.Mock.AddCall(call)
	return Iface_MockSet_Method{Call: call}
}

// Args sets the exact values of the arguments.
func (_s Iface_MockSet_Method) Args(i int64) Iface_MockSet_Method {
	_s.Call.Args[0] = i
	return _s
}

// ArgsAnything sets the interface values of the arguments.
func (_s Iface_MockSet_Method) ArgsAnything(i interface{}) Iface_MockSet_Method {
	_s.Call.Args[0] = i
	return _s
}

// Arg_i_Matcher sets matcher of the i argument value.
func (_s Iface_MockSet_Method) Arg_i_Matcher(matcher func(i int64) bool) Iface_MockSet_Method {
	realMatcher := func(arg interface{}) bool {
		value, ok := arg.(int64)
		if !ok {
			return false
		}
		return matcher(value)
	}

	_s.Call.Args[0] = realMatcher
	return _s
}

// Rets sets the exact values of the result parameters.
func (_s Iface_MockSet_Method) Rets(_r0 error) Iface_MockSet_Method {
	_s.Call.Returns[0] = _r0
	return _s
}

// Times sets number of times to call this caller of the method.
func (_s Iface_MockSet_Method) Times(times int) Iface_MockSet_Method {
	_s.Call.Times = times
	return _s
}

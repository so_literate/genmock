// Package genmock is a core of the mock system.
// You can use it as is (see ExampleMock) to mock any your object,
// but it is better to use the generator of the test objects.
// If you will be use generator you can handle errors of
// argument types in compile code stage instead panic in runtime.
// This mock returns error instead of panic,
// so you can handle error and stop you tests correctly.
package genmock

package genmock_test

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/so_literate/genmock"
)

// Data in the database.
type Data struct {
	ID   int
	Data string
}

// DB describes methods of the real object.
type DB interface {
	SetData(data *Data) error
	GetData(id int64) (*Data, error)
	Clean()
}

// DBMock your own type that implements DB interface.
type DBMock struct {
	mock *genmock.Mock
	t    genmock.TestingT // you can pass there a (t *testing.T).
}

func (db *DBMock) SetData(data *Data) error {
	db.t.Helper()
	ret, err := db.mock.MethodCalled("SetData", data)
	if err != nil {
		db.t.Fatalf("db.MethodCalled: %s", err)
	}

	res, err := genmock.ConvertArgError(0, ret)
	if err != nil {
		db.t.Fatalf("genmock.ConvertArgError (SetData): %s", err)
	}

	return res
}

func (db *DBMock) GetData(id int64) (*Data, error) {
	db.t.Helper()
	ret, err := db.mock.MethodCalled("GetData", id)
	if err != nil {
		db.t.Fatalf("db.MethodCalled: %s", err)
	}

	data, ok := ret[0].(*Data)
	if !ok {
		db.t.Fatalf("wrong type of the return argument: %#v", ret[0])
	}

	resErr, err := genmock.ConvertArgError(1, ret)
	if err != nil {
		db.t.Fatalf("genmock.ConvertArgError (GetData): %s", err)
	}

	return data, resErr
}

func (db *DBMock) Clean() {
	db.t.Helper()
	_, err := db.mock.MethodCalled("Clean")
	if err != nil {
		db.t.Fatalf("db.MethodCalled: %s", err)
	}
}

// dbCodeUser code that uses DB interface.
func dbCodeUser(db DB) (*Data, error) {
	err := db.SetData(&Data{Data: "some text"})
	if err != nil {
		return nil, fmt.Errorf("db.SetData: %w", err)
	}

	data, err := db.GetData(1)
	if err != nil {
		return nil, fmt.Errorf("db.GetData: %w", err)
	}

	db.Clean()

	return data, nil
}

func Example_withoutCodeGen() {
	mock := genmock.NewMock()

	matcher := func(arg interface{}) bool {
		data := arg.(*Data)
		return data.Data != ""
	}

	mock.
		AddCall(genmock.NewCall(
			"SetData",
			[]interface{}{matcher},    // First argument with custom compare.
			[]interface{}{error(nil)}, // Return nil as error.
			genmock.TimesAny,          // You can call it any times.
		)).
		AddCall(genmock.NewCall(
			"GetData",
			[]interface{}{genmock.AnythingOfType("int64")}, // First argument with soft compare by type only.
			[]interface{}{&Data{Data: "text"}, nil},        // Returns data and nil as error.
			1,                                              // You can call it only 1 time.
		))

	cleanCall := genmock.NewCall("Clean", nil, nil, 1)
	cleanCall.WaitTime = time.Millisecond * 10 // Wait 10 millisecond when mock found this call.

	mock.AddCall(cleanCall)

	db := &DBMock{
		mock: mock,
		t:    genmock.NewLoggerHelper(log.Default()), // pass *testing.T here.
	}

	data, err := dbCodeUser(db)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = db.mock.AssertExpectations() // Check that all method was called.

	fmt.Println(data.Data)
	fmt.Println(err)

	// Output:
	// text
	// <nil>
}

package genmock_test

import (
	"errors"
	"testing"

	"gitlab.com/so_literate/genmock"
)

func TestIsArgumentListsEquals(t *testing.T) {
	t.Parallel()

	testCase := func(expArgs, actualArgs []interface{}, expErr error) {
		t.Helper()
		err := genmock.IsArgumentListsEquals(expArgs, actualArgs)
		if !errors.Is(err, expErr) {
			t.Fatalf("unexpected error:\n\twant: %v\n\tgot:  %v", expErr, err)
		}
	}

	testCase([]interface{}{"text", 2}, []interface{}{"text", 2}, nil)
	testCase([]interface{}{"text", 2}, []interface{}{"text"}, genmock.ErrLengthNotEqual)
	testCase([]interface{}{"text", 2}, []interface{}{"text", int64(2)}, genmock.ErrDeepEqualNotTrue)

	matcher := func(arg interface{}) bool { return arg.(int) == 3 }

	testCase([]interface{}{matcher}, []interface{}{3}, nil)
	testCase([]interface{}{matcher}, []interface{}{2}, genmock.ErrIsEqualNotTrue)
}

func TestIsArgumentListsSimilar(t *testing.T) {
	t.Parallel()

	testCase := func(expArgs, actualArgs []interface{}, expErr error) {
		t.Helper()
		err := genmock.IsArgumentListsSimilar(expArgs, actualArgs)
		if !errors.Is(err, expErr) {
			t.Fatalf("unexpected error (%v - %v):\n\twant: %v\n\tgot:  %v", expArgs, actualArgs, expErr, err)
		}
	}

	testCase([]interface{}{genmock.Anything, 2}, []interface{}{"text", 2}, nil)
	testCase([]interface{}{genmock.AnythingOfType("string"), 2}, []interface{}{"text", 2}, nil)
	testCase([]interface{}{genmock.AnythingOfType("string"), 2}, []interface{}{1, 2}, genmock.ErrUnexpectedArgumentType)

	testCase([]interface{}{genmock.AnythingOfType("string"), 2}, []interface{}{nil, 2}, genmock.ErrArgumentTypeUnknown)

	testCase([]interface{}{"text", 2}, []interface{}{"text", 2}, nil)
	testCase([]interface{}{"text", 2}, []interface{}{"text"}, genmock.ErrLengthNotEqual)
	testCase([]interface{}{"text", 2}, []interface{}{"text", int64(2)}, genmock.ErrDeepEqualNotTrue)

	// You can't compare interfaces like this.
	var err error = genmock.ErrWrongReturnedLenght //nolint:stylecheck // pass ErrWrongReturnedLenght as error interface.
	testCase([]interface{}{genmock.AnythingOfType("error")}, []interface{}{err}, genmock.ErrUnexpectedArgumentType)
	// Only this.
	testCase([]interface{}{genmock.AnythingOfType("*errors.errorString")}, []interface{}{err}, nil)
	testCase([]interface{}{genmock.AnythingOfType("*errors.errorString")}, []interface{}{genmock.ErrWrongReturnedLenght}, nil)
}

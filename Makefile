.PHONY: lints lints_fix go_gen test coverage

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml
	cd ./cmd/genmock && golangci-lint run -c ../../.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml
	cd ./cmd/genmock && golangci-lint run --fix -c ../../.linters.yml

go_gen:
	go generate ./_example/codegen/
	go generate ./_example/testcases/

test:
	go test -cover ./...
	cd ./cmd/genmock && make test

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

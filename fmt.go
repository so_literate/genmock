package genmock

import (
	"fmt"
	"strings"
)

func fmtSliceInterfaces(vals []interface{}) string {
	res := make([]string, len(vals))
	for i, val := range vals {
		res[i] = fmt.Sprintf("%[1]T(%#[1]v)", val)
	}

	return strings.Join(res, ", ")
}

func fmtErrorWithList(err error, list []string) error {
	if len(list) == 0 {
		return err
	}
	return fmt.Errorf("%w\n\t- %s", err, strings.Join(list, "\n\t- "))
}

func fmtErrorIndexOutOfRange(length, index int) error {
	return fmt.Errorf("%w: length %d, index %d", ErrIndexOutOfRange, length, index)
}

func fmtErrorUnexpectedArgumentType(wantType string, arg interface{}) error {
	return fmt.Errorf("%w: want %q, got: %[3]T(%#[3]v)", ErrUnexpectedArgumentType, wantType, arg)
}
